<html>

<!--#include virtual="/inc/config.asp"-->
<!--#include virtual="/inc/dbcon.asp"-->
<%


site = check_sql(Request("site"))
t = check_sql(Request("t"))

Function getTimeStamp()
	Dim intSeconds, intMilliseconds, strMilliseconds, intDatePart, intTimePart
	
	intSeconds = (Hour(Now) * 3600) + (Minute(Now) * 60) + Second(Now)
	intMilliseconds = Timer() - intSeconds
	intMilliseconds = Fix(intMilliseconds * 100)
	
	intDatePart = (Year(Now) * 10000) + (Month(Now) * 100) + Day(Now)
	intTimePart = (Hour(Now) * 1000000) + (Minute(Now) * 10000) + (Second(Now) * 100) + intMilliseconds
	
	getTimeStamp = intDatePart & intTimePart 
   'getTimeStamp = intDatePart & " " & intTimePart 
End Function
%>
<html>
 <head>
  <title> <%=site%> 알 충환전 내역 </title>
  <meta name="Generator" content="EditPlus">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
 </head>

<link href="css/main.css?t=<%=getTimeStamp()%>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/js/iscroll.js"></script>
<script type="text/javascript">
<!--
	var qScroll = null;

	$(document).ready(function(){

		var temp = new Date();
		temp.setDate(temp.getDate());
		var y = temp.getFullYear();
		var m = temp.getMonth() + 1;
		var d = temp.getDate();

		var tmpPdate = temp.getFullYear() + '-'
             + ('0' + (temp.getMonth()+1)).slice(-2) + '-'
             +  ('0' + temp.getDate()).slice(-2);

		var temp = new Date();
		//temp.setDate(temp.getDate()-2);
		var y = temp.getFullYear();
		var m = temp.getMonth() + 1;
		var d = temp.getDate();

		var tmpPdate1 = temp.getFullYear() + '-'
             + ('0' + (temp.getMonth()+1)).slice(-2) + '-'
              +  ('01');// + temp.getDate()).slice(-2);

		$("#sd1").val(tmpPdate1);
		$("#sd2").val(tmpPdate);

		
		ddd();
		_getal();

		$(".rf").unbind("click").bind("click",function(){
			ddd();
			_getal();
		});

		$("#moreBtn").unbind("click").bind("click",function(){

			ddd("more");
			_getal();
		}) ;
	});


	var ddd = function(t){
		$("#tscroller").empty();

		if(qScroll != null){
			qScroll.destroy();
		}

		qScroll = new IScroll('#twrapper', { 
				scrollbars: true,
				mouseWheel: true,
				interactiveScrollbars: true,
				shrinkScrollbars: 'scale',
				fadeScrollbars: false,
				click: true
		});

		var obj = {
			"sd":$("#sd1").val(),
			"fd":$("#sd2").val(),
			"site":"<%=site%>"
		}
			
		if (t =="more"){
			t="more";

		}		
	
		$.post('/api/get_al_history.asp?t='+t ,obj,function(data){
			
			data = JSON.parse(data);
			console.log(data);
			if(data.length > 0 ){
				for(var key in data){

					var tmpstr = "<div class = 'al_num center'>" + data[key]["rk"] + "</div>";
					tmpstr = tmpstr + "<div class = 'al_desc '>" + chkDW(data[key]["al_desc"]) + "</div>";
					tmpstr = tmpstr + "<div class = 'al_amount '>" + chk1(numberWithCommas(data[key]["al_amount"]),data[key]["type"]) + "</div>";
					tmpstr = tmpstr + "<div class = 'al_curr '>" + (numberWithCommas(data[key]["al_curr"])) + "</div>";
					tmpstr = tmpstr + "<div class = 'al_dt '>" + data[key]["d1"] + "  " + data[key]["d2"] + "</div>";


					$("#tscroller").append("<div class = 'tlist'>" + tmpstr + "</div>");
				}
			}else{
				$("#tscroller").append("<div class = 'tlist'>내역이 없습니다.</div>");
			}

			$("#tscroller").height(data.length*31 + 50);

			qScroll.refresh();
		});
	}

	var chk1 = function(str,type){

		if(type == "1"){
			return "<font color = 'red'>-" + str + "</font>";
		}
	
		if(type == "2"){
			return "<font color = 'blue'>+" + str + "</font>";
		}
	}
	
	var chkDW = function(str){

		if(str.indexOf("지급") > -1){
			str = str.replace("지급","<font color = 'red'>지급</font>"); 
		}
		if(str.indexOf("차감") > -1){
			str = str.replace("차감","<font color = 'blue'>차감</font>"); 
		}
		if(str.indexOf("충전") > -1){
			str = str.replace("충전","<font color = 'red'>충전</font>"); 
		}
		if(str.indexOf("환전") > -1){
			str = str.replace("환전","<font color = 'blue'>환전</font>"); 
		}


		return str
	}

	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	


	var _getal = function(){
		$.get("/api/getal_q.asp?site=<%=site%>",function(data){
			$(".alll").text(numberWithCommas(data));
		});
	}

	var altimer = null;

//-->
</script>
<style>
#twrapper {
    position: absolute;
    z-index: 1;
    top: 169px;
    bottom: 48px;
    left: -1px;
    width: 802px;
    background: transparent;
    overflow: hidden;
    height: 645px;
    color: black;
    font-size: 11px;
    background: rgba(128, 128, 128, 0.3);
}
#moreBtn {
    position: absolute;
    z-index: 1;
    top: 818px;
    bottom: 48px;
    left: -1px;
    width: 802px;
    background: transparent;
    overflow: hidden;
    height: 35px;
    color: black;
    font-size: 20px;
    background: rgba(128, 128, 128, 0.3);
}
#tscroller{

}
.tlist{
	float:left;
	width:100%;
	height:30px;
	border-bottom: 1px solid rgba(255, 255, 255, 0.29);
}
.al_num{
    float: left;
    width: 70px;
    height: 31px;
    font-size: 19px;
	    text-align: center;
	    border-right: 1px solid rgba(255, 255, 255, 0.10);

}
.al_desc{
	float: left;
    width: 232px;
    height: 25px;
    font-size: 15px;
    border-right: 1px solid rgba(255, 255, 255, 0.10);
   
    padding-top: 5px;
    padding-left: 10px;

}
.al_amount{
		float: left;
    width: 132px;
    height: 25px;
    font-size: 15px;
    border-right: 1px solid rgba(255, 255, 255, 0.10);
    font-weight: bold;
    padding-top: 5px;
    padding-right: 10px;
	text-align:right;
}
.al_curr{
		float: left;
    width: 132px;
    height: 25px;
    font-size: 15px;
    border-right: 1px solid rgba(255, 255, 255, 0.10);
    font-weight: bold;
    padding-top: 5px;
    padding-right: 10px;
	text-align:right;
	    color: #57587b;
}

.al_dt{
    float: left;
    width: 178px;
    height: 20px;
    font-size: 12px;
    border-right: 1px solid rgba(255, 255, 255, 0.10);
    /* font-weight: bold; */
    padding-top: 10px;
    padding-right: 10px;
    text-align: right;
}

.ttile{
	font-weight:bold !important;
	
    font-size: 15px !important;
	    padding-top: 5px !important;
}
.rf:hover{
	opacity:0.9;
	cursor:pointer;
}

.s_d{
	float: left;
    font-size: 12px;
    background: rgba(0, 0, 255, 0.24);
    color: black;
    padding: 2px;
    margin-left: 10px;
}

.s_d1{
    float: left;
    width: 115px;
    font-size: 11px;
    margin-left: 3px;
    height: 18px;
}
</style>
<body style = "background:rgba(0, 0, 0, 0);">

<div style = "    float: left;
    width: 100%;
    padding: 20px;
    font-size: 27px;
    font-weight: bold;
    background: #f5c40b;
    color: white;
    text-shadow: 0px 0px 2px black;    TEXT-ALIGN: CENTER;" class = 'center'> <%=SITE%> 알 충환전 내역</div>
	<div style = "     float: left;
    width: 100%;
    margin-top: 54px;
    height: 31px;
    background: #c0c0da;">
		<div class="tlist">
			<div class="al_num center ttile "  style = "    padding-top: 1px !important;">순서</div>
			<div class="al_desc center ttile">내용</div>
			<div class="al_amount center ttile ">변동금액</div>
			<div class="al_curr  center ttile">금액</div>
			<div class="al_dt center ttile ">날짜</div>
		</div>
	</div>
	<div id="twrapper">
		<div id="tscroller">

		</div>
	</div>


<div style = "    position: absolute;
    top: 84px;
    background: rgba(0, 0, 255, 0.22);
    color: black;
    height: 32px;
    width: 106px;" class = "center">현재 보유 알</div>

	<div style = "     position: absolute;
    top: 84px;
    background: rgba(0, 0, 255, 0.22);
    color: black;
    height: 24px;
    width: 206px;
    left: 107px;
    padding-top: 8px;
    text-align: right;
    padding-right: 20px;" class = "alll"></div>

	<div style = "    position: absolute;
    top: 84px;
    background: rgba(12, 12, 232, 0.68);
    color: white;
    height: 24px;
    width: 69px;
    left: 705px;
    padding-top: 8px;
    text-align: center;
    font-weight: bold;
    border-radius: 6px;" class = "rf">Refresh</div>

	<fieldset style = "    position: absolute;
    top: 74px;
    left: 344px;
    height: 29px;
    width: 314px;">
		<legend>기간 검색</legend>
		<div class = 's_d'>시작</div>
		<input type ="date" class = 's_d1' id = "sd1">
		<div class = 's_d'>종료</div>
		<input type ="date" class = 's_d1' id = "sd2">
	</fieldset>
		<div><button id="moreBtn">더보기</button>
		</div>

</body>
</html>