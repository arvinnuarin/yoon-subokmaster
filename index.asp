﻿<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>
 

<div class="w3-bar w3-black">
  <button class="w3-bar-item w3-button" onclick="openCity('cs')">CS</button>
  <button class="w3-bar-item w3-button" onclick="openCity('message')">메세지</button>
  <button class="w3-bar-item w3-button" onclick="openCity('al')">알 관리</button>
  <button class="w3-bar-item w3-button" onclick="openCity('88_ag')">88 & ag 영상 관리</button>
  <button class="w3-bar-item w3-button" onclick="openCity('chanel')">영상 채널 관리</button>
  <button class="w3-bar-item w3-button" onclick="openCity('shoe')">슈정보</button>  
  <button class="w3-bar-item w3-button" onclick="openCity('room')">방관리</button>
</div>

<div id="cs" class="w3-container city">
  

	 

	<div class="dropdown">
	  <button class="dropbtn">Dropdown</button>
	  <div class="dropdown-content">
		<a href="#">Link 1</a>
		<a href="#">Link 2</a>
		<a href="#">Link 3</a>
	  </div>
	</div>
			



</div>

<div id="message" class="w3-container city" style="display:none">
  <h2>London</h2>
  <p>London is the capital city of England.</p>
</div>

<div id="al" class="w3-container city" style="display:none">
  <h2>Paris</h2>
  <p>Paris is the capital of France.</p> 
</div>

<div id="88_ag" class="w3-container city" style="display:none">
  <h2>Tokyo</h2>
  <p>Tokyo is the capital of Japan.</p>
</div>

<div id="chanel" class="w3-container city" style="display:none">
  <h2>Tokyo</h2>
  <p>Tokyo is the capital of Japan.</p>
</div>

<div id="shoe" class="w3-container city" style="display:none">
  <h2>Tokyo</h2>
  <p>Tokyo is the capital of Japan.</p>
</div>

 

<div id="room" class="w3-container city" style="display:none">
  <h2>Tokyo</h2>
  <p>Tokyo is the capital of Japan.</p>
</div>

<script>

$( document ).ready(function() {
    console.log( "ready!" );
});
 
function openCity(cityName) {
  var i;
  var x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  document.getElementById(cityName).style.display = "block";  

}
</script>

</body>
</html>
