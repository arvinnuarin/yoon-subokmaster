﻿<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
 
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" />  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>  
<script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>

<style>
 
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
 
#body_div{
	width:100%;
	height:96%;
	

	background-image:url('/img/loading.gif');
	background-repeat:no-repeat;
	background-position:center center;
	background-color:rgba(0, 0, 0, 0.7);
	z-index:100;
	position:relative;
}

#center_div{
	
	top:-95%;
	max-height:96%;
	position:relative;
}	
#data_div{
	max-height:0%;
    margin-bottom:4%;
	opacity:0;  
}   
</style>
</head>
<body >
	<input style="float:left; margin-left:5px; margin-top:3px; width: 100px; text-align:center;" class="form-control" type="text" id="testDatepicker" readonly/> 
	<button  style="margin-top:3px;" type="button" id="s_bt" class="btn btn-danger btn-md">search</button>


</body>
<body style= "height:100%;">
<div id="body_div"></div>
<div id="center_div">
	<div id="data_div" style="margin-top:10px;">
		
	</div>
</div>
</body>
</html>

 
<script type="text/javascript">  
	 
	$( document ).ready(function() {	
 
		$('#body_div').animate({opacity: "0"}, 4000);
		var now = new Date();

		var year= now.getFullYear();
		var mon = (now.getMonth()+1)>9 ? ''+(now.getMonth()+1) : '0'+(now.getMonth()+1);
		var day = now.getDate()>9 ? ''+now.getDate() : '0'+now.getDate();
              
		var chan_val = year + '-' + mon + '-' + day;
 

		$( "#testDatepicker" ).val(chan_val);
		$(function() {
			$( "#testDatepicker" ).datepicker({
				dateFormat:"yy-mm-dd",
				maxDate:0
			});
		});

		get_all_cal('today');
		
	});
		

function numberWithCommas(x){if(x==undefined){x="";}
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",");
}


function get_all_cal(s_date){
	 
	 $.get('/api/get_all_cal.asp?s_date='+s_date,function(data){
					

			data = JSON.parse(data);
			 
			

			var alsum=0;
			var all="all";
			var tmp ="<table id='customers'>";
			    tmp =tmp + "<tr>";
				//tmp =tmp + "<th style='text-align:center;'>상태</th>";
				;
				tmp =tmp + "<th style='text-align:center;'>고객사</th>";
				tmp =tmp + "<th style='text-align:center;'>입금</th>";
				tmp =tmp + "<th style='text-align:center;'>출금</th>";
				tmp =tmp + "<th style='text-align:center;'>정산된 입금</th>";
				tmp =tmp + "<th style='text-align:center;'>정산된 출금</th>";
				tmp =tmp + "<th style='text-align:center;'>테스트 계정- "
							+ "<span><button style='float:middle;'  class='btn btn-del btn-md' type='button' onclick='testIdDel(&#39;"+all+"&#39;)'>전체삭제</button></span></th>";
				tmp =tmp + "</tr>";
 
				var testtt=true;	
			
			var tmp_button="";

			for(var key in data){
				var confirm="X";
				 //alert(data[key]['total_deposit2']);
				if(testtt){
					//data[key]['total_deposit']="23423432";
				}
				 testtt=false;

				if((data[key]['total_deposit']==data[key]['cal_deposit']) &&(data[key]['total_widraw']==data[key]['cal_widraw'])  ){
					tmp =tmp +"<tr>";
					tmp_button="-";
				}else{
					tmp =tmp +"<tr style='background-color:#ff9999'>";
					tmp_button="<span><button style='float:middle;' type='button' onclick='re_cal(&#39;"+data[key]['eng_name']+"&#39;)' class='btn btn-info'>재정산</button></span>";
				}

				//tmp =tmp+"<td align='center'>"+tmp_button+"</td>"; 
				tmp =tmp +"<td align='center'>"+data[key]['kor_name'] + "("+data[key]['eng_name']+")</td>";

				tmp =tmp +"<td align='right'>" + numberWithCommas(data[key]['total_deposit']) +"</td>";
				tmp =tmp +"<td align='right'>" + numberWithCommas(data[key]['total_widraw']) +"</td>";
				tmp =tmp +"<td align='right'>" + numberWithCommas(data[key]['cal_deposit']) +"</td>";
				tmp =tmp +"<td align='right'>" + numberWithCommas(data[key]['cal_widraw']) +"</td>";
				tmp =tmp +"<td align='center'> <span><button style='float:middle;' type='button' onclick='testIdDel(&#39;"+data[key]['eng_name']+"&#39;)' class='btn testIdDel'>삭제</button></span> </td>"; 
			
				tmp =tmp +"</tr>";
				tmp_button="";
			 
			}
			tmp=tmp+"</table>";
 
			$("#data_div").html(tmp);
			$("#data_div").animate({opacity: "1"}, 3000);
 



		});
	 
}


$( "#s_bt" ).click(function() {
	$("#data_div").animate({opacity: "0"}, 1000);
	get_all_cal($("#testDatepicker").val());
	 
	 
});

function re_cal(eng_name){
	 var s_date=$("#testDatepicker").val();
	 return;
	 /*
	 $.get('/api/get_re_cal.asp?s_date='+s_date+'site_name='+eng_name,function(data){

	 });
	 */
}

function testIdDel(eng_name){

		//var inputString = prompt('비밀번호를 입력해주세요'); 

		if(prompt('비밀번호를 입력해주세요') =="789789" ){
			 $.get('/api/testIdDel.asp?site_name='+eng_name ,function(data){

			});
		}else{
			
			alert("비밀번호 확인");

		}

}

function systemDel(eng_name){

		if(prompt('비밀번호를 입력해주세요') =="789789" ){

			$.get('/api/systemDel.asp?site_name='+eng_name ,function(data){

			});
		}else{
			alert("비밀번호 확인");
		}
}
function triggerMake(eng_name){



			$.get('/api/triggerMake.asp?site_name='+eng_name ,function(data){

			});
}
function checkGame(eng_name){



			$.get('/api/checkGame.asp?site_name='+eng_name ,function(data){
			alert(data)				

			});
}
 
</script>
