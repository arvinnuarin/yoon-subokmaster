﻿
<html>
<head>
<%
Function getTimeStamp()
	Dim intSeconds, intMilliseconds, strMilliseconds, intDatePart, intTimePart
	
	intSeconds = (Hour(Now) * 3600) + (Minute(Now) * 60) + Second(Now)
	intMilliseconds = Timer() - intSeconds
	intMilliseconds = Fix(intMilliseconds * 100)
	
	intDatePart = (Year(Now) * 10000) + (Month(Now) * 100) + Day(Now)
	intTimePart = (Hour(Now) * 1000000) + (Minute(Now) * 10000) + (Second(Now) * 100) + intMilliseconds
	
	getTimeStamp = intDatePart & intTimePart 
   'getTimeStamp = intDatePart & " " & intTimePart 
End Function
%>
<title>Casino - Data 검색</title>
<META NAME="Author" CONTENT="MIDAS" />
<meta name="keywords" content="Online,Casino,baccarat" />
<meta name="description" content="Online Casino baccarat" />
<meta name="robots" content="all" />
<meta name="revisit-after" content="1 day" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

 <link href="/ms/dk.css?t=<%=getTimeStamp()%>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/js/iscroll.js"></script>
<script type="text/javascript" src="/ms/dk.js?t=<%=getTimeStamp()%>"></script>

<style>

table.type08 {
    border-collapse: collapse;
    text-align: left;
    line-height: 1.5;
    border-left: 1px solid #ccc;
    margin: 20px 10px;
}

table.type08 thead th {
    padding: 5px;
    font-weight: bold;
    border-top: 1px solid #ccc;
    border-right: 1px solid #ccc;
	background: #dcdcd1;
    
}
table.type08 tbody th {
    width: 150px;
    padding: 5px;
    font-weight: bold;
    vertical-align: top;
    border-right: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
    background: #ececec;
}
table.type08 td {
    width: 350px;
    padding: 5px;
    vertical-align: top;
    border-right: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
}
td,th{
	text-align:center;
	cursor:pointer;
}

th:hover{
	opacity:0.8;
	background:rgba(128, 128, 128, 0.17);
}

td:hover{
	opacity:0.8;
	background:rgba(128, 128, 128, 0.17);
}

.add:hover , .alb:hover{
	opacity:0.8;
}

.open{
	background: #33ab33;
}

.close{
	background: #de4040e3;
}

</style>
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
 <script type="text/javascript">
 
	var txt;

	var getRoomAll = function(){

		$.get('/api/get_room_state_oriental.asp',function(data){
			
			data = JSON.parse(data);
	 
			var for_key=0;
			for(var key in data){
			 
				if(data[key].locked == 0){
					 
					txt = "Open";
					$("tr:eq("+(parseInt(for_key)+2)+") ."+data[key].roomno).addClass("open");
				}else{
				 
					txt = "Close";
					$("tr:eq("+(parseInt(for_key)+2)+") ."+data[key].roomno).addClass("close");
				 
				}
				 
				$("tr:eq("+(parseInt(for_key)+2)+") ."+data[key].roomno).html(txt);

 				if(data[key].roomno=="OG7"){
					for_key++;
				}
				 
			}	
		});

	}

	$(document).ready(function(){
	


		$.get('/api/get_dbInfo.asp?target=ori',function(data){
			
			data = JSON.parse(data);	

			$("tbody").empty();
			for(var key in data){
				
				if(key < 11){
					var tmp = '<Tr class="soo"  > <th class="page" scope="row"data-no = "' +  data[key]['site_url'] + '" >' + data[key]['kor_name'] + '</th>';
				}else{
					var tmp = '<Tr><th class="page" scope="row" >' + data[key]['kor_name'] + '</th>';
				}
				tmp = tmp + '<td class = "OC2" id="OC2" data-type="OC" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OC3" id="OC3" data-type="OC" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OC5" id="OC5" data-type="OC" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OC6" id="OC6" data-type="OC" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OC7" id="OC7" data-type="OC" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OC8" id="OC8" data-type="OC" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OC9" id="OC9" data-type="OC" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OG1" id="OG1" data-type="OG" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N" style="border-left:2px #000 solid;"></td>';
				tmp = tmp + '<td class = "OG2" id="OG2" data-type="OG" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OG3" id="OG3" data-type="OG" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OG5" id="OG5" data-type="OG" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OG6" id="OG6" data-type="OG" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "OG7" id="OG7" data-type="OG" data-name = "' + data[key]['kor_name'] + '" data-no = "' + data[key]['site_url'] + '" data-check="N"></td>';
		


				$("tbody").append(tmp);	
			}
			getRoomAll();
			chgroom();
			chgroomAll();
		});
	});
		$(document).on("click",".page",function(){

			var popUrl = "http://" + $(this).data("no");
			var popOption = "width=1640, height=850, resizable=no, scrollbars=no, status=no;";
			window.open(popUrl, "aa", popOption);

		});
	function chgroom(){
	
		$("tr td").unbind("click").bind("click",function(){
			
			var question = confirm($(this).attr("data-name")+" "+$(this).attr("id")+"번방 상태를 변경하시겠습니까?");

				if(question){
					
					var status
					var gameType
					var command

					if($(this).attr("class").indexOf("open") >-1){
						status = "1"
					}else{
						status = "0"
					}

					gameType=$(this).data("type");
					if(status =="1"){
						command = "doclose"
						$(this).removeClass("open");
					}else{
						command = "doopen"
						$(this).removeClass("close");
					}


						var obj = {
									"gametype":gameType,
									"status":status,
									"roomno":$(this).attr("id"),
									"site":$(this).attr("data-name"),
									"command":command
								}

								$.post('/api/chg_room_status_pg3.asp',obj,function(data){
								
									if(data =="success"){
										alert("성공적으로 변경되었습니다");
									}else{
										alert("변경중 에러가 발생하였습니다");
									}

									getRoomAll();
								
								});
				}
		});

	}

	function chgroomAll(){
	
		$(".allchg").unbind("click").bind("click",function(){

			var question = confirm("수컴 "+$(this).attr("data")+"번방 전체를 닫으시겠습니까?");

			if(question){
					var status
					var gameType
					var roomno
					status = "1"
					gameType=$(this).data("type");
					roomno = "CA_"+$(this).attr("data");
					$(".soo .CA_"+$(this).attr("data")).removeClass("open");
					var obj = {
								"gametype":gameType,
								"status":status,
								"roomno":roomno
							}

							$.post('/api/chg_room_status_all_pg3.asp',obj,function(data){
							
								if(data =="success"){
									alert("성공적으로 변경되었습니다");
								}else{
									alert("변경중 에러가 발생하였습니다");
								}

								getRoomAll();
							
							}); 

			}
		});
	}


 </script>
 <body>

 
<table class="type08" width="95%">
    <thead>
		<tr>
			<th></th>
			<th colspan="7">OC</th>
			<th colspan="6" style="border-left:2px #000 solid;">OG</th>

		</tr>
		 <tr>
			<th width="3%" scope="cols">사이트 명</th>
			<th class="allchg" width="2%" scope="cols" data="OC2" data-type="OC">OC2</th>
			<th class="allchg" width="2%" scope="cols" data="OC3" data-type="OC">OC3</th>
			<th class="allchg" width="2%" scope="cols" data="OC5" data-type="OC">OC5</th>
			<th class="allchg" width="2%" scope="cols" data="OC6" data-type="OC">OC6</th>
			<th class="allchg" width="2%" scope="cols" data="OC7" data-type="OC">OC7</th>
			<th class="allchg" width="2%" scope="cols" data="OC8" data-type="OC">OC8</th>
			<th class="allchg" width="2%" scope="cols" data="OC9" data-type="OC">OC9</th>
			<th class="allchg" width="2%" scope="cols" data="OG1" data-type="OG"style="border-left:2px #000 solid;" >OG1</th>
			<th class="allchg" width="2%" scope="cols" data="OG2" data-type="OG" >OG2</th>
			<th class="allchg" width="2%" scope="cols" data="OG3" data-type="OG">OG3</th>
			<th class="allchg" width="2%" scope="cols" data="OG5" data-type="OG">OG5</th>
			<th class="allchg" width="2%" scope="cols" data="OG6" data-type="OG">OG6</th>
			<th class="allchg" width="2%" scope="cols" data="OG7" data-type="OG">OG7</th>

		</tr>

	</thead>

    <tbody>
	

	</tbody>
</table>


 </body>
 </html>