﻿<%
 Session.CodePage  = 65001 '한글
 Response.CharSet  = "UTF-8" '한글
 Response.AddHeader "Pragma","no-cache"
 Response.AddHeader "cache-control", "no-staff"
 Response.Expires  = -1

'Session("auth") = "1234"


Function Check_sql(ByVal str)  
   Dim result_str
   SQL_Val = str
   SQL_Val = My_ReplaceText(SQL_Val, ";", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "@variable", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "@@variable", " ")
   SQL_Val = Replace(SQL_Val, "+", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "print", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "set", " ")
   SQL_Val = Replace(SQL_Val, "%", " ")
   'SQL_Val = My_ReplaceText(SQL_Val, "<script>", " ")
   'SQL_Val = My_ReplaceText(SQL_Val, "<SCRIPT>", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "script", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "SCRIPT", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "union", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "insert", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "openrowset", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "xp_", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "decare", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "select", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "update", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "delete", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "shutdown", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "drop", " ")
   SQL_Val = Replace(SQL_Val, "--", " ")
   SQL_Val = Replace(SQL_Val, "/*", " ")
   SQL_Val = Replace(SQL_Val, "*/", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "XP_", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "DECLARE", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "SELECT", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "UPDATE", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "DELETE", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "INSERT", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "SHUTDOWN", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "DROP", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "cmdshel", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "exec", " ")
   SQL_Val = Replace(SQL_Val, "<>", " ")

   'SQL_Val = My_ReplaceText(SQL_Val, "and", " ")
	SQL_Val = Replace(SQL_Val, " and", "")
	SQL_Val = Replace(SQL_Val, "and ", "")
   SQL_Val = My_ReplaceText(SQL_Val, "user", " ")
   SQL_Val = My_ReplaceText(SQL_Val, ".asp", " ")
   SQL_Val= replace(SQL_Val, "'", "''")
   SQL_Val= replace(SQL_Val, "'", "|")
   SQL_Val= replace(SQL_Val, "|", "''")
   SQL_Val= replace(SQL_Val, "\""", "|")
   SQL_Val= replace(SQL_Val, "|", "''")
SQL_Val = My_ReplaceText(SQL_Val, ".zip", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "backup", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "database", " ")

   SQL_Val = My_ReplaceText(SQL_Val, "chr", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "char", "-- ")
   SQL_Val = My_ReplaceText(SQL_Val, "sql", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "sqlmap", "-- ")
   SQL_Val = My_ReplaceText(SQL_Val, "output", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "dbo.", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "cast", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "_table", "-- ")
   SQL_Val = My_ReplaceText(SQL_Val, "null", "-- ")
    SQL_Val = My_ReplaceText(SQL_Val, ".sys", " --")

   SQL_Val = My_ReplaceText(SQL_Val, "trunc", "-- ")
   SQL_Val = My_ReplaceText(SQL_Val, "unicode", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "waitfor", " ")
   SQL_Val = My_ReplaceText(SQL_Val, "delay", " ")

   result_str = removeXSS(SQL_Val)
   result_str = removeTableName(result_str)

   Check_sql = result_str 
End Function


%>