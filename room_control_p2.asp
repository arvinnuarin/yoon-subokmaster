﻿
<html>
<head>
<%
Function getTimeStamp()
	Dim intSeconds, intMilliseconds, strMilliseconds, intDatePart, intTimePart
	
	intSeconds = (Hour(Now) * 3600) + (Minute(Now) * 60) + Second(Now)
	intMilliseconds = Timer() - intSeconds
	intMilliseconds = Fix(intMilliseconds * 100)
	
	intDatePart = (Year(Now) * 10000) + (Month(Now) * 100) + Day(Now)
	intTimePart = (Hour(Now) * 1000000) + (Minute(Now) * 10000) + (Second(Now) * 100) + intMilliseconds
	
	getTimeStamp = intDatePart & intTimePart 
   'getTimeStamp = intDatePart & " " & intTimePart 
End Function
%>
<title>Casino - Data 검색</title>
<META NAME="Author" CONTENT="MIDAS" />
<meta name="keywords" content="Online,Casino,baccarat" />
<meta name="description" content="Online Casino baccarat" />
<meta name="robots" content="all" />
<meta name="revisit-after" content="1 day" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

 <link href="/ms/dk.css?t=<%=getTimeStamp()%>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/js/iscroll.js"></script>
<script type="text/javascript" src="/ms/dk.js?t=<%=getTimeStamp()%>"></script>

<style>

table.type08 {
    border-collapse: collapse;
    text-align: left;
    line-height: 1.5;
    border-left: 1px solid #ccc;
    margin: 20px 10px;
}

table.type08 thead th {
    padding: 5px;
    font-weight: bold;
    border-top: 1px solid #ccc;
    border-right: 1px solid #ccc;
    background: #dcdcd1;
}
table.type08 tbody th {
    width: 150px;
    padding: 5px;
    font-weight: bold;
    vertical-align: top;
    border-right: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
    background: #ececec;
}
table.type08 td {
    width: 350px;
    padding: 5px;
    vertical-align: top;
    border-right: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
}
td,th{
	text-align:center;
	cursor:pointer;
}

th:hover{
	opacity:0.8;
	background:rgba(128, 128, 128, 0.17);
}

td:hover{
	opacity:0.8;
	background:rgba(128, 128, 128, 0.17);
}

.add:hover , .alb:hover{
	opacity:0.8;
}

.open{
	background: #33ab33;
}

.close{
	background: #de4040e3;
}

</style>
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
 <script type="text/javascript">
 
	var txt;

	var getRoomAll = function(){

		$.get('/api/get_room_state_sol.asp',function(data){
			data = JSON.parse(data);
	 
			var for_key=0;
			for(var key in data){
			 
				if(data[key].locked == 0){
					 
					txt = "Open";
					$("tr:eq("+(parseInt(for_key)+2)+") ."+data[key].roomno).addClass("open");
				}else{
				 
					txt = "Close";
					$("tr:eq("+(parseInt(for_key)+2)+") ."+data[key].roomno).addClass("close");
				 
				}
				 
				$("tr:eq("+(parseInt(for_key)+2)+") ."+data[key].roomno).html(txt);

 				if(data[key].roomno=="CA_8829"){
					for_key++;
				}
				 
			}	
		});

		

		$.get('/api/get_room_state_vg_new.asp',function(data){
			
			data = JSON.parse(data);
	 
			var for_key=0;
			for(var key in data){
			 
				if(data[key].locked == 0){
					 
					txt = "Open";
					$("tr:eq("+(parseInt(for_key)+2)+") ."+data[key].roomno).addClass("open");
				}else{
				 
					txt = "Close";
					$("tr:eq("+(parseInt(for_key)+2)+") ."+data[key].roomno).addClass("close");
				 
				}
				 
				$("tr:eq("+(parseInt(for_key)+2)+") ."+data[key].roomno).html(txt);

 				if(data[key].roomno=="CA_8828"){
					for_key++;
				}
				 
			}	
		});

	}

	$(document).ready(function(){
	
		$.get('/api/getal.asp?target=all',function(data){
			
			data = JSON.parse(data);	

				$("tbody").empty();
				for(var key in data){
					
					if(key < 11){
						var tmp = '<Tr class="soo"><th scope="row">' + data[key]['kor_name'] + '</th>';
					}else{
						var tmp = '<Tr><th scope="row">' + data[key]['kor_name'] + '</th>';
					}
					tmp = tmp + '<td class = "CA_8801" id="CA_8801" data-type="88" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
					tmp = tmp + '<td class = "CA_8802" id="CA_8802" data-type="88" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
					tmp = tmp + '<td class = "CA_8803" id="CA_8803" data-type="88" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
					tmp = tmp + '<td class = "CA_8804" id="CA_8804" data-type="88" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
					tmp = tmp + '<td class = "CA_8810" id="CA_8810" data-type="88" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
					tmp = tmp + '<td class = "CA_8811" id="CA_8811" data-type="88" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';	
					tmp = tmp + '<td class = "CA_8805" id="CA_8805" data-type="tg" data-name = "' + data[key]['kor_name'] + '" data-check="N" style="border-left:2px #000 solid;"></td>';
					tmp = tmp + '<td class = "CA_8806" id="CA_8806" data-type="tg" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
					tmp = tmp + '<td class = "CA_8807" id="CA_8807" data-type="tg" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
					tmp = tmp + '<td class = "CA_8808" id="CA_8808" data-type="tg" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
					tmp = tmp + '<td class = "CA_8809" id="CA_8809" data-type="tg" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
					tmp = tmp + '<td class = "CA_8829" id="CA_8829" data-type="tg" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';				


					$("tbody").append(tmp);	
				}
				getRoomAll();
				chgroom();
				chgroomAll();
		});
	});

	function chgroom(){
	
		$("tr td").unbind("click").bind("click",function(){
			
			var question = confirm($(this).attr("data-name")+" "+$(this).attr("id")+"번방 상태를 변경하시겠습니까?");

				if(question){
					
					var status
					var gameType
					var command

					if($(this).attr("class").indexOf("open") >-1){
						status = "1"
					}else{
						status = "0"
					}

					gameType=$(this).data("type");
					if(status =="1"){
						command = "doclose"
						$(this).removeClass("open");
					}else{
						command = "doopen"
						$(this).removeClass("close");
					}


						var obj = {
									"gametype":gameType,
									"status":status,
									"roomno":$(this).attr("id"),
									"site":$(this).attr("data-name"),
									"command":command
								}

								$.post('/api/chg_room_status_pg2.asp',obj,function(data){
								
									if(data =="success"){
										alert("성공적으로 변경되었습니다");
									}else{
										alert("변경중 에러가 발생하였습니다");
									}

									getRoomAll();
								
								});
				}
		});

	}

	function chgroomAll(){
	

		$(".allchg").unbind("click").bind("click",function(){

		if(prompt("비밀번호를 입력해 주세요") == "123456"){

			var question = confirm($(this).attr("data")+" 전체를 닫으시겠습니까?");

			if(question){
					var status
					var gameType
					var roomno
					var command ="allclose"
					//var site ="더케이";
				var site="all";

					status = "1"
					gameType=$(this).data("type");

					if ($(this).attr("data") == "allclose"){

						roomno = $(this).attr("data");
						

					}else{

						roomno = "CA_"+$(this).attr("data");						

					}

					$(".soo .CA_"+$(this).attr("data")).removeClass("open");
					var obj = {
								"gametype":gameType,
								"status":status,
								"roomno":roomno,
								"command":command,
								"site":site
							}

							$.post('/api/chg_room_status_pg2.asp',obj,function(data){
							
								if(data =="success"){
									alert("성공적으로 변경되었습니다");
								}else{
									alert("변경중 에러가 발생하였습니다");
								}

								getRoomAll();
							
							}); 

				}
			
		}else{
			alert("다음에 다시 하세요");			
		}	
	});
}



 </script>
 <body>

 
<table class="type08" width="95%">
    <thead>
		<tr>
			<th></th>
			<th colspan="6">88</th>
			<th colspan="6" style="border-left:2px #000 solid;">Dragon & Tiger</th>
		</tr>
		 <tr>
			<th width="1%" scope="cols">사이트 명</th>
			<th class="allchg" width="2%" scope="cols" data="8801" data-type="88">8801</th>
			<th class="allchg" width="2%" scope="cols" data="8802" data-type="88">8802</th>
			<th class="allchg" width="2%" scope="cols" data="8803" data-type="88">8803</th>
			<th class="allchg" width="2%" scope="cols" data="8804" data-type="88">8804</th>
			<th class="allchg" width="2%" scope="cols" data="8810" data-type="88">8810</th>
			<th class="allchg" width="2%" scope="cols" data="8811" data-type="88">8811</th>
			<th class="allchg" width="2%" scope="cols" data="8805" data-type="tg" style="border-left:2px #000 solid;">8805</th>
			<th class="allchg" width="2%" scope="cols" data="8806" data-type="tg">8806</th>
			<th class="allchg" width="2%" scope="cols" data="8807" data-type="tg">8807</th>
			<th class="allchg" width="2%" scope="cols" data="8808" data-type="tg">8808</th>
			<th class="allchg" width="2%" scope="cols" data="8809" data-type="tg">8809</th>
			<th class="allchg" width="2%" scope="cols" data="8829" data-type="tg">8829</th>
			<th class="allchg" width="2%" scope="cols" data="allclose" data-type="allClose88" style="background: #e08dbb;">88 및 용호 전체닫기</th>

		</tr>
	</thead>

    <tbody>
	

	</tbody>
</table>


 </body>
 </html>