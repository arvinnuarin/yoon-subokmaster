﻿
<html>
<head>
<%
Function getTimeStamp()
	Dim intSeconds, intMilliseconds, strMilliseconds, intDatePart, intTimePart
	
	intSeconds = (Hour(Now) * 3600) + (Minute(Now) * 60) + Second(Now)
	intMilliseconds = Timer() - intSeconds
	intMilliseconds = Fix(intMilliseconds * 100)
	
	intDatePart = (Year(Now) * 10000) + (Month(Now) * 100) + Day(Now)
	intTimePart = (Hour(Now) * 1000000) + (Minute(Now) * 10000) + (Second(Now) * 100) + intMilliseconds
	
	getTimeStamp = intDatePart & intTimePart 
   'getTimeStamp = intDatePart & " " & intTimePart 
End Function
%>
<title>Casino - Data 검색</title>
<META NAME="Author" CONTENT="MIDAS" />
<meta name="keywords" content="Online,Casino,baccarat" />
<meta name="description" content="Online Casino baccarat" />
<meta name="robots" content="all" />
<meta name="revisit-after" content="1 day" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

 <link href="/ms/dk.css?t=<%=getTimeStamp()%>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="/js/iscroll.js"></script>
<script type="text/javascript" src="/ms/dk.js?t=<%=getTimeStamp()%>"></script>

<style>

table.type08 {
    border-collapse: collapse;
    text-align: left;
    line-height: 1.5;
    border-left: 1px solid #ccc;
    margin: 20px 10px;
}

table.type08 thead th {
    padding: 5px;
    font-weight: bold;
    border-top: 1px solid #ccc;
    border-right: 1px solid #ccc;
    background: #dcdcd1;
}
table.type08 tbody th {
    width: 150px;
    padding: 5px;
    font-weight: bold;
    vertical-align: top;
    border-right: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
    background: #ececec;
}
table.type08 td {
    width: 350px;
    padding: 5px;
    vertical-align: top;
    border-right: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
}
td,th{
	text-align:center;
	cursor:pointer;
}

th:hover{
	opacity:0.8;
	background:rgba(128, 128, 128, 0.17);
}

td:hover{
	opacity:0.8;
	background:rgba(128, 128, 128, 0.17);
}

.add:hover , .alb:hover{
	opacity:0.8;
}

.open{
	background: #33ab33;
}

.close{
	background: #de4040e3;
}

</style>
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
 <script type="text/javascript">
 
	var txt;

	var getRoomAll = function(){

		$.get('/api/get_room_state_md.asp',function(data){
			data = JSON.parse(data);
	 
			var for_key=0;
			for(var key in data){
			 
				if(data[key].locked == 0){
					 
					txt = "Open";
					$("tr:eq("+(parseInt(for_key)+2)+") .md"+data[key].roomno).addClass("open");
				}else{
				 
					txt = "Close";
					$("tr:eq("+(parseInt(for_key)+2)+") .md"+data[key].roomno).addClass("close");
				 
				}
				 
				$("tr:eq("+(parseInt(for_key)+2)+") .md"+data[key].roomno).html(txt);

 				if(data[key].roomno==18){
					for_key++;
				}
				 
			}	
		});

		

		$.get('/api/get_room_state_mdi.asp',function(data){
			
			data = JSON.parse(data);
	 
			var for_key=0;
			for(var key in data){
			 
				if(data[key].locked == 0){
					 
					txt = "Open";
					$("tr:eq("+(parseInt(for_key)+2)+") .mdi"+data[key].roomno).addClass("open");
				}else{
				 
					txt = "Close";
					$("tr:eq("+(parseInt(for_key)+2)+") .mdi"+data[key].roomno).addClass("close");
				 
				}
				 
				$("tr:eq("+(parseInt(for_key)+2)+") .mdi"+data[key].roomno).html(txt);

 				if(data[key].roomno==32){
					for_key++;
				}
				 
			}	
		});

	}

	$(document).ready(function(){

			$("tbody").empty();
		


		$.get('/api/getal.asp?target=all',function(data){
			
			data = JSON.parse(data);




			for(var key in data){
				
				if(key < 11){
					var tmp = '<Tr class="soo"><th scope="row">' + data[key]['kor_name'] + '</th>';
				}else{
					var tmp = '<Tr><th scope="row">' + data[key]['kor_name'] + '</th>';
				}

				
				tmp = tmp + '<td class = "md1" id="1" data-alertthis="md1"     data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md2" id="2" data-alertthis="md2"     data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md3" id="3" data-alertthis="md3"     data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md5" id="5" data-alertthis="md5"    data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md6" id="6" data-alertthis="md6"     data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md7" id="7" data-alertthis="md7"    data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md8" id="8" data-alertthis="md8"    data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md9" id="9" data-alertthis="md9"    data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md10" id="10" data-alertthis="md10"    data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md11" id="11" data-alertthis="md11"    data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md12" id="12" data-alertthis="md12"    data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md13" id="13" data-alertthis="md13"  data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md15" id="15" data-alertthis="md15"  data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md16" id="16" data-alertthis="md16"  data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md17" id="17" data-alertthis="md17"  data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "md18" id="18" data-alertthis="md18"  data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "mdi25" id="25" data-alertthis="mdi1" data-name = "' + data[key]['kor_name'] + '" data-check="N" style="border-left:2px #000 solid;"></td>';
				tmp = tmp + '<td class = "mdi26" id="26" data-alertthis="mdi2" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "mdi27" id="27" data-alertthis="mdi3" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "mdi28" id="28" data-alertthis="mdi5" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "mdi29" id="29" data-alertthis="mdi6" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "mdi30" id="30" data-alertthis="mdi7" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "mdi31" id="31" data-alertthis="mdi8" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';
				tmp = tmp + '<td class = "mdi32" id="32" data-alertthis="mdi9" data-name = "' + data[key]['kor_name'] + '" data-check="N"></td>';		


				$("tbody").append(tmp);	
			}
			getRoomAll();
			chgroom();
			chgroomAll();
		});
	});

	function chgroom(){
	
		$("tr td").unbind("click").bind("click",function(){
			var question = confirm($(this).attr("data-name")+" "+$(this).data("alertthis")+"번방 상태를 변경하시겠습니까?");

				if(question){
					
					var status
					var gameType
					var command

					if($(this).attr("class").indexOf("open") >-1){
						status = "1"
					}else{
						status = "0"
					}

					if($(this).attr("id") > 24){
						gameType="mdi"
					}else{
						gameType="midas"
					}	
					
					if(status =="1"){
						command = "doclose"
						$(this).removeClass("open");
					}else{
						command = "doopen"
						$(this).removeClass("close");
					}


						var obj = {
									"gametype":gameType,
									"status":status,
									"roomno":$(this).attr("id"),
									"site":$(this).attr("data-name"),
									"command":command
								
								}
									console.log(obj);
								$.post('/api/chg_room_status_pg1.asp',obj,function(data){
								
									if(data =="success"){
										alert("성공적으로 변경되었습니다");
									}else{
										alert("변경중 에러가 발생하였습니다");
									}

									getRoomAll();
								
								});
				}
		});

	}

	//마이다스 및 인터네셔널 방 모두 닫기

	function chgroomAll(){

		$(".allchg").unbind("click").bind("click",function(){

		if(prompt("비밀번호를 입력해 주세요") == "123456"){

			var question = confirm( $(this).data("name")+"번방 전체를 닫으시겠습니까?");

			if(question){

					var status
					var gameType
					var roomno
					var command 

					//var site ="더케이";
				var site="all";

					status = "1"
				
					if($(this).attr("data") == "allmd" ){
						gameType="midas";	
						command="allcloseMd";					

					}else if($(this).attr("data") == "allmdi" ){
						gameType="mdi";
						command="allcloseMdi";	

					}else if($(this).attr("data") > 24){
						gameType="mdi";
						command="roomcloseMdi";	
					}else{
						gameType="midas";
						command="roomcloseMd";	
					}	
					
					roomno = $(this).attr("data");
					
					if(gameType == "midas"){
						$(".soo .md"+$(this).attr("data")).removeClass("open");
					}else{
						$(".soo .mdi"+$(this).attr("data")).removeClass("open");
					}


					var obj = {
								"gametype":gameType,
								"status":status,
								"roomno":roomno,
								"command":command,
								"site":site
							}

							$.post('/api/chg_room_status_pg1.asp',obj,function(data){
							
								if(data =="success"){
									alert("성공적으로 변경되었습니다");
								}else{
									alert("변경중 에러가 발생하였습니다");
								}

								getRoomAll();
							
							}); 

			}
		}else{
			alert("다음에 다시 하세요");			
		}	
	});
}

/*
	var data = [
		
		//수컴
		{"name":"모나코","domain":"aaad.mnco1004.com"},
		{"name":"구솔레어","domain":"aaadsol.mnco1004.com"},
		{"name":"갤럭시","domain":"aaad.ggg-777.com"},
		{"name":"마이다스","domain":"aaad.mdsc8888.com"},
		{"name":"엘리트","domain":"aaad.alt9999.com"},	
		{"name":"스타월드","domain":"aaad.starw999.com"},
		{"name":"위더스","domain":"aaad.widus77.com"},
		{"name":"퀸즈","domain":"aaad.queen1004.com"},
		{"name":"솔레어_new","domain":"aaad.sol-88.com"},
		//{"name":"더블카지노","domain":"aaad.dd-77.com"},
		{"name":"이태리","domain":"aaad.ttt-777.com"},

		//타사
		{"name":"3Gun","domain":"aaad.gun3333.com"},
		//{"name":"배트맨","domain":"aaad.btm-1000.com"},
		//{"name":"홈카","domain":"aaad.homeca-00.com"},
		{"name":"글로벌","domain":"aaad.gb-22.com"},
		{"name":"솔레어2","domain":"ardee77.com"},
		{"name":"89","domain":"aaad.8989baca.com"},
		{"name":"럭키9","domain":"aaad.9-luck.com"},
		//{"name":"오아시스","domain":"aaad.oasis00.com"},
		{"name":"지청","domain":"aaad.hghg7979.com"},
		//{"name":"내추럴뱃","domain":"aaad.rds1004.com"},
		{"name":"미라지","domain":"aaad.mira1004.com"},
		//{"name":"미르","domain":"aaad.mir1472.com"},
		{"name":"오리엔탈","domain":"aaad.ort999.com"},
		{"name":"달라스","domain":"aaad.prds777.com"},
		//{"name":"MGM","domain":"aaad.mgm-vvip.com"},
		{"name":"정마담","domain":"aaad.mda999.com"},
		{"name":"메이저","domain":"aaad.maj747.com"},
		{"name":"k2","domain":"aaad.k2game888.com"},
		{"name":"소울","domain":"aaad.soul9999.com"},
		{"name":"탑","domain":"aaad.top-baca.com"},
		{"name":"SBS","domain":"aaad.sbs369.com"},
		{"name":"VIP","domain":"aaad.rvip7.com"},
		//{"name":"나인9","domain":"aaad.nine9-9.com"},
		{"name":"세븐카지노","domain":"aaad.seven7-7.com"},
		//{"name":"스위트","domain":"aaad.st-002.com"},
		{"name":"랜드","domain":"aaad.land0623.com"},
		//{"name":"내추럴9","domain":"aaad.nat9999.com"},
		//{"name":"마이더스2","domain":"aaad.mds345.com"},
		//{"name":"M카지노","domain":"aaad.mca7777.com"},
		{"name":"007","domain":"aaad.007casino.com"},
		//{"name":"믹스나인","domain":"aaad.mix-9.com"},
		{"name":"뉴월드","domain":"aaad.new7777.com"},
		//{"name":"신화","domain":"aaad.sin-777.com"},
		{"name":"에이스","domain":"aaad.ace8989.com"},
		{"name":"에이스2","domain":"aaad.og5566.com"},
		{"name":"코리아","domain":"aaad.kor6688.com"},
		{"name":"드림","domain":"aaad.dream.com"},
		{"name":"엠카","domain":"aaad.mo-mm.com"},
		{"name":"술","domain":"aaad.sool79.com"},
		{"name":"바보","domain":"aaad.babo-777.com"},
		{"name":"MGM_new","domain":"aaad.ppbb123.com"},
		{"name":"로데오","domain":"aaad.rode5.com"},
		{"name":"midas777","domain":"aaad.midas777.com"},
		{"name":"시티","domain":"aaad.city1111.com"},
		{"name":"골드문","domain":"aaad.gdm-777.com"},
		{"name":"팡팡","domain":"aaad.pb727.com"},
		
		{"name":"펄","domain":"aaad.pearl8253.com"},
	]
*/

 </script>
 <body>

 
<table class="type08" width="95%">
    <thead>
		<tr>
			<th></th>
			<th colspan="16">Midas</th>
			<th colspan="8" style="border-left:2px #000 solid;">Midas International</th>
		</tr>
		 <tr>
			<th width="3%" scope="cols">사이트 명</th>
			<th class="allchg" width="2%" scope="cols" data="1" data-name="MD1">MD1</th>
			<th class="allchg" width="2%" scope="cols" data="2" data-name="MD2">MD2</th>
			<th class="allchg" width="2%" scope="cols" data="3" data-name="MD3">MD3</th>
			<th class="allchg" width="2%" scope="cols" data="5" data-name="MD5">MD5</th>
			<th class="allchg" width="2%" scope="cols" data="6" data-name="MD6">MD6</th>
			<th class="allchg" width="2%" scope="cols" data="7" data-name="MD7">MD7</th>
			<th class="allchg" width="2%" scope="cols" data="8" data-name="MD8">MD8</th>
			<th class="allchg" width="2%" scope="cols" data="9" data-name="MD9">MD9</th>
			<th class="allchg" width="2%" scope="cols" data="10" data-name="MD10">MD10</th>
			<th class="allchg" width="2%" scope="cols" data="11" data-name="MD11">MD11</th>
			<th class="allchg" width="2%" scope="cols" data="12" data-name="MD12">MD12</th>
			<th class="allchg" width="2%" scope="cols" data="13" data-name="MD13">MD13</th>
			<th class="allchg" width="2%" scope="cols" data="15" data-name="MD15">MD15</th>
			<th class="allchg" width="2%" scope="cols" data="16" data-name="MD16">MD16</th>
			<th class="allchg" width="2%" scope="cols" data="17" data-name="MD17">MD17</th>
			<th class="allchg" width="2%" scope="cols" data="18" data-name="MD18">MD18</th>
			<th class="allchg" style="border-left:2px #000 solid;" width="2%" scope="cols" data="25" data-name="MDI1">MDI1</th>
			<th class="allchg" width="2%" scope="cols" data="26" data-name="MDI2">MDI2</th>
			<th class="allchg" width="2%" scope="cols" data="27" data-name="MDI3">MDI3</th>
			<th class="allchg" width="2%" scope="cols" data="28" data-name="MDI5">MDI5</th>
			<th class="allchg" width="2%" scope="cols" data="29" data-name="MDI6">MDI6</th>
			<th class="allchg" width="2%" scope="cols" data="30" data-name="MDI7">MDI7</th>
			<th class="allchg" width="2%" scope="cols" data="31" data-name="MDI8">MDI8</th> 
			<th class="allchg" width="2%" scope="cols" data="32" data-name="MDI9">MDI9</th>
			<th class="allchg" width="2%" scope="cols" data="allmd" data-name="allCloseMd" style="background: #e08dbb;" >마이다스 전체닫기</th>
			<th class="allchg" width="2%" scope="cols" data="allmdi" data-name="allCloseMdi" style="background: #e08dbb;" >인터네셔널 전체닫기</th>
		</tr>
	</thead>

    <tbody>
	

	</tbody>
</table>


 </body>
 </html>