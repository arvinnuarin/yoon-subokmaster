﻿ <!--#include virtual="/inc/JSON_2.0.4.asp"-->
<!--#include virtual="/inc/config.asp"-->
<!--#include virtual="/inc/dbcon.asp"-->

<html>

<%

'If Request.ServerVariables("HTTP_X_REAL_IP") = "111.67.211.177" Or Request.ServerVariables("HTTP_X_REAL_IP") = "175.111.17.196" Or Request.ServerVariables("HTTP_X_REAL_IP") = "220.230.78.21"  Or Request.ServerVariables("remote_addr") = "111.67.211.177" Or Request.ServerVariables("remote_addr") = "175.111.17.196" Or Request.ServerVariables("remote_addr") = "220.230.78.21" Or Request.ServerVariables("remote_addr") = "103.9.129.79" Or Request.ServerVariables("HTTP_X_REAL_IP") = "103.9.129.79" Then

'Else
'	Response.Status = "404 Not found"
'	Response.End
'End If



site = check_sql(Request("site"))
work = check_sql(Request("work"))

Function getTimeStamp()
	Dim intSeconds, intMilliseconds, strMilliseconds, intDatePart, intTimePart
	
	intSeconds = (Hour(Now) * 3600) + (Minute(Now) * 60) + Second(Now)
	intMilliseconds = Timer() - intSeconds
	intMilliseconds = Fix(intMilliseconds * 100)
	
	intDatePart = (Year(Now) * 10000) + (Month(Now) * 100) + Day(Now)
	intTimePart = (Hour(Now) * 1000000) + (Minute(Now) * 10000) + (Second(Now) * 100) + intMilliseconds
	
	getTimeStamp = intDatePart & intTimePart 
   'getTimeStamp = intDatePart & " " & intTimePart 
End Function
%>
<html>
 <head>
  <title> <%=site%> <%=work%> </title>
  <meta name="Generator" content="EditPlus">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">
 </head>

<link href="css/main.css?t=<%=getTimeStamp()%>" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.11.3.js"></script>

<script type="text/javascript">

</script>
<style>
#twrapper {
    position: absolute;
    z-index: 1;
    top: 156px;
    bottom: 48px;
    left: -1px;
    width: 802px;
    background: transparent;
    overflow: hidden;
    height: 645px;
    color: black;
    font-size: 11px;
    background: rgba(128, 128, 128, 0.3);
}

#tscroller{

}
.tlist{
	float:left;
	width:100%;
	height:30px;
	border-bottom: 1px solid rgba(255, 255, 255, 0.29);
}
.al_num{
    float: left;
    width: 70px;
    height: 31px;
    font-size: 19px;
	    text-align: center;
	    border-right: 1px solid rgba(255, 255, 255, 0.10);

}
.al_desc{
	float: left;
    width: 232px;
    height: 25px;
    font-size: 15px;
    border-right: 1px solid rgba(255, 255, 255, 0.10);
   
    padding-top: 5px;
    padding-left: 10px;

}
.al_amount{
		float: left;
    width: 132px;
    height: 25px;
    font-size: 15px;
    border-right: 1px solid rgba(255, 255, 255, 0.10);
    font-weight: bold;
    padding-top: 5px;
    padding-right: 10px;
	text-align:right;
}
.al_curr{
		float: left;
    width: 132px;
    height: 25px;
    font-size: 15px;
    border-right: 1px solid rgba(255, 255, 255, 0.10);
    font-weight: bold;
    padding-top: 5px;
    padding-right: 10px;
	text-align:right;
	    color: #57587b;
}

.al_dt{
    float: left;
    width: 178px;
    height: 20px;
    font-size: 12px;
    border-right: 1px solid rgba(255, 255, 255, 0.10);
    /* font-weight: bold; */
    padding-top: 10px;
    padding-right: 10px;
    text-align: right;
}

.ttile{
	font-weight:bold !important;
	
    font-size: 15px !important;
	    padding-top: 5px !important;
}
.rf:hover{
	opacity:0.9;
	cursor:pointer;
}

.s_d{
	float: left;
    font-size: 12px;
    background: rgba(0, 0, 255, 0.24);
    color: black;
    padding: 2px;
    margin-left: 10px;
}

.s_d1{
    float: left;
    width: 115px;
    font-size: 11px;
    margin-left: 3px;
    height: 18px;
}

.head th{
	font-weight:500;
	border:1px solid #80808059;
}

#layertbody tr td, #layertbody2 tr td , #layertbody3 tr td , #layertbody4 tr td , #layertbody5 tr td, #layertbody6 tr td, #layertbody7 tr td, #layertbody10 tr td, #layertbodyRoomrow tr td, #layertbodydepo tr td{
	font-size:14px;
	border:1px solid #80808059;
	text-align:center;
	height: 47px;
}

.null{
	background:#e6f7594d;
}

.selectedLine{
	background:#84cde8c7;
}

#layertbody > tr:hover, #layertbodydepo > tr:hover{

	-webkit-transition:all .5s ease;
	font-weight:500;
	cursor:pointer;
	background:#84cde8e8;

}

.regDim{
    position: fixed;
    top: 0px;
    left: 0px;
    height: 100%;
    z-index: 1000;
	display:none;
}

.selectedOrder{
	background:#84cde8c7;
}



</style>
<script type="text/javascript">


	$(document).ready(function(){

		$("#betlog").hide();
		$("#roomrow").hide();
		$("#modiamount").hide();
		$("#memberaccount").hide();		
		$("#deluser").hide();

		$("#userdeposit").hide();		
		$("#agentwidraw").hide();
	
		$("#calmiss").hide();
		
		if("<%=work%>" == "배팅로그조회"){

			gameChange('betlog');
//			setDate();
			
			$("#betlog .searchBtn").unbind("click").bind("click",function(){
				searchLog();
			});
			

			$("#betlog").show();
			
		}else if("<%=work%>" == "회차결과내역"){
 
			gameChange('roomrow');
			$("#roomrow .searchBtn3").unbind("click").bind("click",function(){
				searchRoomrow();
			});			
			$("#roomrow").show();


		}else if("<%=work%>" == "보유금수정"){

			
			$("#modiamount").show();

			$(".searchBtn2").unbind("click").bind("click",function(){
				
				localStorage.setItem('guid',0);
				localStorage.setItem('deposit',0);
				localStorage.setItem('depodate',0);
				localStorage.setItem('widraw',0);
				localStorage.setItem('widdate',0);
				localStorage.setItem('pre_acc',0);
				localStorage.setItem('after_acc',0);
				localStorage.setItem('winprice',0);
				localStorage.setItem('betdate',0);

				searchUser();
			});

		}else if("<%=work%>" == "회원삭제"){

			
			$("#deluser").show();

			$(".searchBtn5").unbind("click").bind("click",function(){
				
				searchDelUser();
			});

		}else if("<%=work%>" == "회원계좌조회"){

			
			$("#memberaccount").show();

			$(".searchBtn7").unbind("click").bind("click",function(){

				searchAccount();
			});

		}else if("<%=work%>" == "회원입출금내역"){	
			

			$(".searchBtn4").unbind("click").bind("click",function(){
				searchDepoWid();
			});			


			$("#userdeposit").show();


		}else if("<%=work%>" == "에이전트출금내역"){
			$("#roomrow").show();
		}else if("<%=work%>" == "정산누락내역"){
			$("#roomrow").show();
		}
		

	});
	
	/////////////////////배팅로그조회 START////////////////////

	function gameChange(menuid){

		gameOrder(menuid);

		var gameType =  $("#"+menuid+" .gameType option:selected").val();

			$(".added").remove();
			
			if(gameType == "midas"){
				var j = 0;
				for(var i=1; i<=16; i++){
					j = i;
					if(i >= 4)
						j = j + 1;
					
					if(i >= 13)
						j = j + 1;

					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");
				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(i)
								.addClass("added")
								.html("md-"+j);
	
				}
			
			}else if(gameType == "88"){
				var j = 0;
				var roomorderTxt;
				for(var i=1; i<=6; i++){
					j = i;
					
					if(i == 5)
						j = 10;
					else if(i == 6)
						j = 11;
					
					if(i < 5){
						roomorderTxt = "CA_880"+j;
					}else if(i == 5){
						roomorderTxt = "CA_8810";
					}else if(i == 6){
						roomorderTxt = "CA_8811";
					}					
					
					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");
				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(i)
								.addClass("added")
								.html(roomorderTxt);
				}
				
			}else if(gameType == "dragon"){
				var j = 0;
				var roomorderTxt;
				for(var i=1; i<=6; i++){
					j = i + 4;
					

					
					if(i == 6){
						roomorderTxt = "CA_8829";
					}else{
						roomorderTxt = "CA_880"+j;
					}
					
					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");
				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(roomorderTxt)
								.addClass("added")
								.html(roomorderTxt);
				}
				
			}else if(gameType == "mdi"){
				var j = 0;
				var roomorderTxt;
				for(var i=1; i<=8; i++){
					j = i + 24;
					

			
						roomorderTxt = j;
					
					
					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");
				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(roomorderTxt)
								.addClass("added")
								.html(roomorderTxt);
				}
				
			}else if(gameType == "cagayan"){

				var txt = "C00";
	
				for(var i=1; i<=4; i++){

					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");
				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(i)
								.addClass("added")
								.html(txt+i);
	
				}

			}else if(gameType == "solaire"){

				var txt = "A00";
	
				for(var i=1; i<=4; i++){				
				
					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");
				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(i)
								.addClass("added")
								.html(txt+i);
	
				}

			}else if(gameType == "okada"){

				var txt = "T00";
	
				for(var i=1; i<=5; i++){
			

				
					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(i)
								.addClass("added")
								.html(txt+i);
	
				}

			}else if(gameType == "cod"){

				var txt = "D00";
	
				for(var i=1; i<=4; i++){
			

				
					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(i)
								.addClass("added")
								.html(txt+i);
	
				}

			}else if(gameType == "OG"){
				var j = 0;
				var txt = "OG";

				for(var i=1; i<=6; i++){
					j = i;
					if(i >= 4)
						j = j + 1;
					
					
					
					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");
				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(j)
								.addClass("added")
								.html(txt+j);
				}
				
			}
			else if(gameType == "EVO"){
				var j = 0;
				var txt = "EV";

				for(var i=1; i<=8; i++){
					j = i;
					
					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");
				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(j)
								.addClass("added")
								.html(txt+j);
				}	
			}
			else if(gameType == "PP"){
		
				var txt = "PP";

				var j = 0;
				for(var i=1; i<=8; i++){
					j = i;
					if(i >= 4)
						j = j + 1;
					
					$("#roomAll"+menuid).clone().appendTo("#"+menuid+" .roomNo");
				
					$("#"+menuid+" .roomNo").find("option:eq("+i+")")
								.attr("id","room"+i)
								.val(j)
								.addClass("added")
								.html(txt+j);
				}	
			}
	}
	function gameOrder(menuid){
		
		var gameType =  $("#"+menuid+" .gameType option:selected").val();
		var roomNo = $("#"+menuid+" .roomNo option:selected").val();

		if(gameType =="88"){

			if(roomNo != "" && roomNo < 5){
				roomNo = "CA_880"+roomNo;
			}else if(roomNo == 5){
				roomNo = "CA_8810";
			}else if(roomNo == 6){
				roomNo = "CA_8811";
			}
		}else if(gameType =="dragon"){
			if(roomNo == 6){
				roomNo = "CA_8829";
			}else{
				roomNo = "CA_880"+roomNo;
			}

		}else if(gameType =="cagayan"){
			if(roomNo != "")
				roomNo = "C00"+roomNo;
		}else if(gameType =="solaire"){
			if(roomNo != "")
				roomNo = "A00"+roomNo;
		}else if(gameType =="cod"){
			if(roomNo != "")
				roomNo = "D00"+roomNo;
		}else if(gameType == "okada"){
			if(roomNo != "")
				roomNo = "T00"+roomNo;			
		}else if(gameType == "OG"){
			if(roomNo != "" && roomNo > 3)
				roomNo = parseInt(roomNo) + 1
				roomNo = "OG"+roomNo;			
		}
		else if(gameType == "EVO"){
			if(roomNo != "")
				roomNo = "EV"+roomNo;
		}
		else if(gameType =="PP"){
			if(roomNo != "")
				roomNo = "PP"+roomNo
		}

			var obj = {
			"gameType":$("#"+menuid+" .gameType option:selected").val(),
			"roomNo":roomNo,
			"site":"<%=site%>",
			"work":"<%=work%>",
			"task":"orderSearch"
		}
	
		$.post('/api/get_cs_work.asp',obj,function(data){
			
			data = JSON.parse(data);

			$(".addList").remove();

			if(data.length > 0 ){
				for(var key in data){
					
					$("#gameAll"+menuid).clone()
								 .appendTo("#"+menuid+" .gameNo")
								 .attr("id","gamelist"+key)
								 .addClass("addList")
								 .html(data[key]["roomorder"])
								 .val(data[key]["roomorder"]);

				}
			}
		});
	}

	function setDate(){

		var temp = new Date();
		temp.setDate(temp.getDate());
		var y = temp.getFullYear();
		var m = temp.getMonth() + 1;
		var d = temp.getDate();

		var tmpPdate = temp.getFullYear() + '-'
             + ('0' + (temp.getMonth()+1)).slice(-2) + '-'
             +  ('0' + temp.getDate()).slice(-2);

		var temp = new Date();
		temp.setDate(temp.getDate()-2);
		var y = temp.getFullYear();
		var m = temp.getMonth() + 1;
		var d = temp.getDate();

		var tmpPdate1 = temp.getFullYear() + '-'
             + ('0' + (temp.getMonth()+1)).slice(-2) + '-'
              +  '01';

		$("#sd1").val(tmpPdate);
		$("#sd2").val(tmpPdate);

	}

	function searchLog(){

	
		var gameType = $(".gameType option:selected").val();
		var roomNo = $(".roomNo option:selected").val();
		var top = $(".top option:selected").val();

		if(gameType =="midas" || gameType =="mdi"){
			
		}else if(gameType =="88"){

			if(roomNo != "" && roomNo < 5){
				roomNo = "CA_880"+roomNo;
			}else if(roomNo == 5){
				roomNo = "CA_8810";
			}else if(roomNo == 6){
				roomNo = "CA_8811";
			}
		}else if(gameType =="dragon"){
	
		}else if(gameType =="OG"){
			if(roomNo != "")
				roomNo = "OG"+roomNo
		}
		else if(gameType =="EVO"){
			if(roomNo != "")
				roomNo = "EV"+roomNo
		}
		else if(gameType =="PP"){
			if(roomNo != "")
				roomNo = "PP"+roomNo
		}



		var obj = {
			"gameType":$(".gameType option:selected").val(),
			"top":top,
			"roomNo":roomNo,
			"sortType":$(".sortType option:selected").val(),
			"gameNo":$(".gameNo option:selected").val(),
			"startDt":$("#sd1").val(),
			"endDt":$("#sd2").val(),
			"userId":$("#userId").val(),
			"roomOrder":$("#condRoomOrder").val(),
			"site":"<%=site%>",
			"work":"<%=work%>"
		}

		console.log(obj)
	
		$.post('/api/get_cs_work.asp',obj,function(data){
			
			data = JSON.parse(data);
			var tmp = "";

			$("tr[id*=list]").remove();
			$("tr[id*=empty]").remove();

			if(data.length > 0 ){
				for(var key in data){
					
					$("#clonebody").clone()
								   .attr("id","list"+key)
								   .appendTo("#layertbody");



					var result
					var resultDt
					var winPrice
					
					//밑줄긋기
					if(data[key]["roomorder"] != tmp && data[key]["roomorder"] != ""  ){
						

						$("#list"+key+" > td ").css("border-top" , "2px solid #e29522" );

					}
						tmp =data[key]["roomorder"];


					if(data[key]["result"] ==""){
						result = "null";
						$("#list"+key+" .column6").addClass("null");
					}else{
						result = data[key]["result"];
					}

					if(data[key]["resultdate"] ==""){
						resultDt = "null";
						$("#list"+key+" .column8").addClass("null");
					}else{
						resultDt = data[key]["resultdate"];
					}

					if(data[key]["winprice"] ==""){
						winPrice = "-";
						$("#list"+key+" .column13").addClass("null");
					}else{
						winPrice = data[key]["winprice"];
					}
					$("#list"+key+" .column0").text(data[key]["id"]);
					$("#list"+key+" .column1").text(data[key]["idx"]);
					$("#list"+key+" .column2").text(data[key]["guid"]);
					
					if(gameType=="midas"){					
	
							if (parseInt(data[key]["roomno"]) > 3 ){

								$("#list"+key+" .column3").text(parseInt(data[key]["roomno"]) + 1 );
							}else{
								$("#list"+key+" .column3").text(data[key]["roomno"]);

							}
					}
					
					/*else if (gameType=="OG") {

						let roomno = data[key]["roomno"].toString().substring(2)

						if (parseInt(roomno) > 4 ){
							$("#list"+key+" .column3").text(`OG${parseInt(roomno) + 1}`);
						}else{
							$("#list"+key+" .column3").text(data[key]["roomno"]);

						}
					} */

					else{
							
							$("#list"+key+" .column3").text(data[key]["roomno"]);
					}


					$("#list"+key+" .column4").text(data[key]["roomorder"]);
					$("#list"+key+" .column5").text(data[key]["bet"]);
					$("#list"+key+" .column6").text(result);
					$("#list"+key+" .column7").text(data[key]["reqdate"]);
					$("#list"+key+" .column8").text(resultDt);
					$("#list"+key+" .column9").text(data[key]["betamount"]);
					$("#list"+key+" .column10").text(data[key]["state"]);
					$("#list"+key+" .column11").text(data[key]["pre_acc"]);
					$("#list"+key+" .column12").text(data[key]["after_acc"]);
					$("#list"+key+" .column13").text(winPrice);
					$("#list"+key+" .column14").text(data[key]["bat"]);
					$("#list"+key+" .column15").text(data[key]["q"]);
					$("#list"+key+" .column16").text(data[key]["rdate"]);



					$("#list"+key).unbind("click").bind("click",function(){
				
						if($(this).hasClass("selectedLine")){
							$(this).removeClass("selectedLine");
						}else{
							$(this).addClass("selectedLine");
						}

					})

					$("#list"+key+" .column4").unbind("click").bind("click",function(){
						
						if($(this).hasClass("selectedOrder")){
							$(this).removeClass("selectedOrder");
						}else{
							$(this).addClass("selectedOrder");
						}

						roomOrderDetail();

					});
					





				}

				$("tr[id*=list]").removeAttr("style");

			}else{

					$("#clonebody").clone()
								   .attr("id","empty")
								   .appendTo("#layertbody");

					$("#empty .column0").text("데이터가 없습니다")
										.attr("colspan","17")
										.css("text-align","center");

					$("tr[id*=empty]").removeAttr("style");

					for(var i=1; i<17; i++){
		
						$("#empty .column"+i).remove();

					}
				}
			});

			$(".excelDownBtn").unbind("click").bind("click",function(){
				excelDown(obj);
			});


	}

	function excelDown(obj){

	var gameType = obj.gameType;
	var top		 = obj.top;
	var roomNo   = obj.roomNo;
	var sortType = obj.sortType;
	var gameNo   = obj.gameNo 
	var startDt  = obj.startDt;
	var endDt    = obj.endDt;
	var userId   = obj.userId;
	var roomOrder   = obj.roomOrder;
	var site     = obj.site;
	var work     = obj.work;

	

		if(confirm(' 배팅로그를 다운받으시겠습니까?')){
		location.href = "/api/get_cs_work.asp?top="+top+"&gameType="+gameType+"&roomNo="+roomNo+"&sortType="+sortType+"&gameNo="+gameNo+"&startDt="+startDt+"&endDt="+endDt+"&userId="+userId+"&roomOrder="+roomOrder+"&site="+site+"&work="+work+"&task=excelDown";

		}
		

	}

	function roomOrderDetail(){

		var obj = {
					"gameType":$("#betlog .gameType option:selected").val(),
					"roomOrder":$(".selectedOrder").html(),
					"task":"searchRoomOrder",
					"site":"<%=site%>",
					"work":"<%=work%>"
				  }


			$.post('/api/get_cs_work.asp',obj,function(data){
			
			data = JSON.parse(data);

			$("tr[id*=lis5t]").remove();
			$("tr[id*=empt5y]").remove();

				if(data.length > 0 ){
					for(var key in data){
						
						$("#clonebody6").clone()
									   .attr("id","lis5t"+key)
									   .appendTo("#layertbody6");


						if(gameType=="midas"){					
		
							if (parseInt(data[key]["roomno"]) > 3 ){

								$("#lis5t"+key+" .column0").text(parseInt(data[key]["roomno"]) + 1 );
							}else{
								
								$("#lis5t"+key+" .column0").text(parseInt(data[key]["roomno"]) + 1 );
							}

						}else{
								
								$("#lis5t"+key+" .column0").text(data[key]["roomno"]);
						}


								$("#lis5t"+key+" .column1").text(data[key]["data"]);
								$("#lis5t"+key+" .column2").text(data[key]["roomorder"]);
								$("#lis5t"+key+" .column3").text(data[key]["result"]);
								$("#lis5t"+key+" .column4").text(data[key]["state"]);
								$("#lis5t"+key+" .column5").text(data[key]["card"]);
								$("#lis5t"+key+" .column6").text(data[key]["reqdate"]);

					}

				$("#betlog tr[id*=lis5t]").removeAttr("style");

			}else{

					$("#clonebody6").clone()
								   .attr("id","empt5y")
								   .appendTo("#layertbody6");

					$("#empt5y .column0").text("데이터가 없습니다.")
										.attr("colspan","7")
										.css("text-align","center");

					$("tr[id*=empt5y]").removeAttr("style");

					for(var i=1; i<7; i++){
						$("#empt5y .column"+i).remove();
					}


					$("tr[id*=empt5y]").removeAttr("style");

				}



				$(".regDim").fadeIn(300,function(){
						$("#layertbody .column2,.column5,.column6,.column7,.column8,.column9,.column10,.column11,.column12,.column13,.column14,.column15,.column16").unbind("click").bind("click",function(){
							$(".regDim").fadeOut(300);
							$(".selectedOrder").removeClass("selectedOrder");
						})
				});

			});
						
	}	
	
	/////////////////////배팅로그조회@ END//////////////////////
	
	/////////////////////회차결과내역 START//////////////////////
	function searchRoomrow(){
	
		var gameType = $("#roomrow .gameType option:selected").val();
		var roomNo = $("#roomrow .roomNo option:selected").val();

		if(gameType =="midas" || gameType =="mdi"){
			
		}else if(gameType =="88"){

			if(roomNo != "" && roomNo < 5){
				roomNo = "CA_880"+roomNo;
			}else if(roomNo == 5){
				roomNo = "CA_8810";
			}else if(roomNo == 6){
				roomNo = "CA_8811";
			}
		}else if(gameType =="dragon"){
		

		}else if(gameType =="OG"){
			if(roomNo != "" && roomNo > 3 )
				roomNo = parseInt(roomNo) + 1
				roomNo = "OG"+roomNo
		}

		var obj = {
			"gameType":$("#roomrow .gameType option:selected").val(),
			"roomNo":roomNo,			
			"gameNo":$("#roomrow .gameNo option:selected").val(),	
			"roomOrder":$("#condRoomOrderRoomrow").val(),
			"site":"<%=site%>",
			"work":"<%=work%>"
		}
	
		$.post('/api/get_cs_work.asp',obj,function(data){
			
			data = JSON.parse(data);

			$("tr[id*=list]").remove();
			$("tr[id*=empty]").remove();

			if(data.length > 0 ){
				for(var key in data){
					
					$("#clonebodyRoomrow").clone()
								   .attr("id","list"+key)
								   .appendTo("#layertbodyRoomrow");



					var cdata,result,card;
					
					if(data[key]["data"] ==""){
						cdata = "null";
						$("#list"+key+" .column1").addClass("null");
					}else{
						cdata = data[key]["data"];
					}

					if(data[key]["result"] ==""){
						result = "null";
						$("#list"+key+" .column4").addClass("null");
					}else{
						result = data[key]["result"];
					}

					if(data[key]["card"] ==""){
						card = "null";
						$("#list"+key+" .column6").addClass("null");
					}else{
						card = data[key]["card"];
					}

	


					if(gameType=="midas"){					
	
							if (parseInt(data[key]["roomno"]) > 3 ){

								$("#list"+key+" .column0").text(parseInt(data[key]["roomno"]) + 1 );
							}else{
								$("#list"+key+" .column0").text(parseInt(data[key]["roomno"]) + 1 );

							}
					}else{
							
							$("#list"+key+" .column0").text(data[key]["roomno"]);
					}


							$("#list"+key+" .column1").text(cdata);
							$("#list"+key+" .column2").text(data[key]["reqdate"]);
							$("#list"+key+" .column3").text(data[key]["roomorder"]);
							$("#list"+key+" .column4").text(result);
							$("#list"+key+" .column5").text(data[key]["state"]);
							$("#list"+key+" .column6").text(card);

				}

				$("tr[id*=list]").removeAttr("style");

			}else{

					$("#clonebodyRoomrow").clone()
								   .attr("id","empty")
								   .appendTo("#layertbodyRoomrow");

					$("#empty .column0").text("데이터가 없습니다")
										.attr("colspan","7")
										.css("text-align","center");

					$("tr[id*=empty]").removeAttr("style");

					for(var i=1; i<8; i++){
		
						$("#empty .column"+i).remove();

					}
				}
			});


	}	
	
	/////////////////////회차결과내역 END//////////////////////

	/////////////////////보유금수정 START//////////////////////

	function searchUser(){
	
	var userId

		if($("#userId2").val() == ""){
			userId = "null";
		}else{
			userId = $("#userId2").val();
		}

		
		var obj = {
			"userId":userId,
			"task":"searchUser",
			"site":"<%=site%>",
			"work":"<%=work%>"
		}

		$.post('/api/get_cs_work.asp',obj,function(data){
			
			data = JSON.parse(data);

			$("tr[id*=list]").remove();
			$("tr[id*=empty]").remove();

				if(data.length > 0 ){
					for(var key in data){
						
						$("#clonebody2").clone()
									   .attr("id","list"+key)
									   .appendTo("#layertbody2");
									
						$("#list"+key+" .column0").text(data[key]["id"]);
						$("#list"+key+" .column1").text(data[key]["guid"]);
						$("#list"+key+" .column2").text(data[key]["nickname"]);
						$("#list"+key+" .column3 #userAmount").val(data[key]["amount"]);
						$("#list"+key+" .column3 #userAmount").attr("id","userAmountchg");

						localStorage.setItem('id',data[key]["id"]);
						localStorage.setItem('guid',data[key]["guid"]);
						localStorage.setItem('examount',data[key]["amount"]);

					}

				$("tr[id*=list]").removeAttr("style");

			}else{

					$("#clonebody2").clone()
								   .attr("id","empty")
								   .appendTo("#layertbody2");

					$("#empty .column0").text("해당유저가 없습니다.")
										.attr("colspan","4")
										.css("text-align","center");

					$("tr[id*=empty]").removeAttr("style");

					for(var i=1; i<4; i++){
						$("#empty .column"+i).remove();
					}
				}
			});

			searchDepo();
			searchWid();
			searchBet();
			
	}


	function calculate(){
		
		var deposit   = parseInt(localStorage.getItem('deposit'));
		var depodate  = localStorage.getItem('depodate');
		
		var widraw    = parseInt(localStorage.getItem('widraw'));
		var widdate   = localStorage.getItem('widdate');

		var pre_acc   = parseInt(localStorage.getItem('pre_acc'));
		var after_acc =	parseInt(localStorage.getItem('after_acc'));
		var winprice  =	parseInt(localStorage.getItem('winprice'));
		var betdate   =	localStorage.getItem('betdate');

		var txt = "보유금계산 : ";
		var caledPrice

		if( betdate > depodate && betdate > widdate ){ //배팅후 입출금 둘다 없는경우
			caledPrice = after_acc+winprice;
			txt = txt + addComma(after_acc) + " + " + addComma(winprice) + " = " + (addComma(caledPrice));

		}else if( betdate > depodate && betdate < widdate) { // 배팅후 출금만 있는경우
			caledPrice = after_acc+winprice-widraw;
			txt = txt + addComma(after_acc) + " + " + addComma(winprice) + " - " + addComma(widraw) +  " = " + (addComma(caledPrice));

		}else if( betdate < depodate && betdate > widdate) { // 배팅후 입금이 있는경우
			caledPrice = after_acc+winprice+deposit;
			txt = txt + addComma(after_acc) + " + " + addComma(winprice) + " + " + addComma(deposit) +  " = " + (addComma(caledPrice));

		}else if( betdate < depodate && betdate < widdate) { // 배팅후 입금 출금이 있는경우
			caledPrice = after_acc+winprice+deposit-widraw;
			txt = txt + addComma(after_acc) + " + " + addComma(winprice) + " + " + addComma(deposit) + " - " + addComma(widraw) +  " = " + (addComma(caledPrice));

		}

		txt = txt +("원");

		$("#calculate").html(txt);

		//amountUpdate();

		
		
	}


	function amountUpdate(){

		$(".modiAmountBtn").unbind("click").bind("click",function(){

				var id = localStorage.getItem('id');
				var guid = localStorage.getItem('guid');
				var examount = localStorage.getItem('examount');
				var chgamount = $("#userAmountchg").val();

				var question = confirm(id+"의 보유금을 수정하시겠습니까? "+addComma(examount)+" ==> "+addComma(chgamount));

				if(question){

						var obj = {
							"guid":guid,
							"chgamount":chgamount,
							"task":"updateAmount",
							"site":"<%=site%>",
							"work":"<%=work%>"
						}

						$.post('/api/get_cs_work.asp',obj,function(data){

							if(data == "success[]"){
								alert("성공적으로 수정하였습니다.");
								searchUser();
							}else{
								alert("수정중 오류가 발생하였습니다.");
							}
						});
		
				}else{
			}
		});



	}

	
	function searchDepo(){

		if($("#userId2").val() == ""){
			userId = "null";
		}else{
			userId = $("#userId2").val();
		}

		
		var obj = {
			"userId":userId,
			"task":"searchDepo",
			"site":"<%=site%>",
			"work":"<%=work%>"
		}

		$.post('/api/get_cs_work.asp',obj,function(data){
				
				data = JSON.parse(data);
		
				$("tr[id*=lis2t]").remove();
				$("tr[id*=empt2y]").remove();
			

					if(data.length > 0 ){
						for(var key in data){
							
							var state

							if(data[key]["state"] =="2"){
								state = "입금완료";
							}
							
							$("#clonebody3").clone()
										   .attr("id","lis2t"+key)
										   .appendTo("#layertbody3");
										
							$("#lis2t"+key+" .column0").text(data[key]["name"]);
							$("#lis2t"+key+" .column1").text(addComma(data[key]["amount"]));
							$("#lis2t"+key+" .column2").text(state);
							$("#lis2t"+key+" .column3").text(data[key]["updatedate"]);

							localStorage.setItem('deposit',data[key]["amount"]);
							localStorage.setItem('depodate',data[key]["updatedate"]);
						
						}

					$("tr[id*=lis2t]").removeAttr("style");


				}else{

						$("#clonebody3").clone()
									   .attr("id","empt2y")
									   .appendTo("#layertbody3");

						$("#empt2y .column0").text("입금내역이 없습니다.")
											.attr("colspan","4")
											.css("text-align","center");

						$("tr[id*=empt2y]").removeAttr("style");

						for(var i=1; i<4; i++){
							$("#empt2y .column"+i).remove();
						}

							localStorage.setItem('deposit',0);
					}
				});
		}



		function searchWid(){

		if($("#userId2").val() == ""){
			userId = "null";
		}else{
			userId = $("#userId2").val();
		}

		
		var obj = {
			"userId":userId,
			"task":"searchWid",
			"site":"<%=site%>",
			"work":"<%=work%>"
		}
			
			
		$.post('/api/get_cs_work.asp',obj,function(data){
				
				data = JSON.parse(data);
		
				$("tr[id*=lis3t]").remove();
				$("tr[id*=empt3y]").remove();
			

					if(data.length > 0 ){
						for(var key in data){
							
							var state

							if(data[key]["state"] =="2"){
								state = "출금완료";
							}
							
							$("#clonebody4").clone()
										   .attr("id","lis3t"+key)
										   .appendTo("#layertbody4");
										
							$("#lis3t"+key+" .column0").text(data[key]["name"]);
							$("#lis3t"+key+" .column1").text(addComma(data[key]["amount"]));
							$("#lis3t"+key+" .column2").text(state);
							$("#lis3t"+key+" .column3").text(data[key]["con_regdate"]);
						
							localStorage.setItem('widraw',data[key]["amount"]);
							localStorage.setItem('widdate',data[key]["con_regdate"]);

						}

					$("tr[id*=lis3t]").removeAttr("style");


				}else{

						$("#clonebody4").clone()
									   .attr("id","empt3y")
									   .appendTo("#layertbody4");

						$("#empt3y .column0").text("출금내역이 없습니다.")
											.attr("colspan","4")
											.css("text-align","center");

						$("tr[id*=empt3y]").removeAttr("style");

						for(var i=1; i<4; i++){
							$("#empt3y .column"+i).remove();
						}

						localStorage.setItem('widraw',0);
					}
				});
		}



		function searchBet(){

		if($("#userId2").val() == ""){
			userId = "null";
		}else{
			userId = $("#userId2").val();
		}

		
		var obj = {
			"userId":userId,
			"task":"searchBet",
			"site":"<%=site%>",
			"work":"<%=work%>"
		}
			
			
		$.post('/api/get_cs_work.asp',obj,function(data){
				
				data = JSON.parse(data);
		
				$("tr[id*=lis4t]").remove();
				$("tr[id*=empt4y]").remove();
			

					if(data.length > 0 ){
						for(var key in data){
							
							var gameName

							if(data[key]["roomno"].indexOf("CA_88") > -1){
								gameName = "88("+data[key]["roomno"]+")";
							}else{
								gameName = "Midas("+data[key]["roomno"]+"번방)";
							}
	

							var winPrice

							if(data[key]["winprice"] == 0){
								winPrice = "-";

							}else if(data[key]["winprice"] == null){
								winPrice = "진행중";
					
							}else{
								winPrice = addComma(data[key]["winprice"]);
							}
							
							
							
							
							$("#clonebody5").clone()
										   .attr("id","lis4t"+key)
										   .appendTo("#layertbody5");
										
							$("#lis4t"+key+" .column0").text(gameName);
							$("#lis4t"+key+" .column1").text(data[key]["roomorder"]);
							$("#lis4t"+key+" .column2").text(addComma(data[key]["betamount"]));
							$("#lis4t"+key+" .column3").text(addComma(data[key]["pre_acc"]));
							$("#lis4t"+key+" .column4").text(addComma(data[key]["after_acc"]));
							$("#lis4t"+key+" .column5").text(winPrice);
							$("#lis4t"+key+" .column6").text(data[key]["rdate"]);

							if(key == 0){
								localStorage.setItem('pre_acc',data[key]["pre_acc"]);
								localStorage.setItem('after_acc',data[key]["after_acc"]);
								localStorage.setItem('winprice',data[key]["winprice"]);
								localStorage.setItem('betdate',data[key]["rdate"]);
							}
						
						}

					$("tr[id*=lis4t]").removeAttr("style");
//					calculate();
					amountUpdate();
				}else{

						$("#clonebody5").clone()
									   .attr("id","empt4y")
									   .appendTo("#layertbody5");

						$("#empt4y .column0").text("배팅내역이 없습니다.")
											.attr("colspan","7")
											.css("text-align","center");

						$("tr[id*=empt4y]").removeAttr("style");

						for(var i=1; i<7; i++){
							$("#empt4y .column"+i).remove();
						}

						localStorage.setItem('pre_acc',0);
						localStorage.setItem('after_acc',0);
						localStorage.setItem('winprice',0);

						amountUpdate();
					}

				});

		}


		function addComma(num) {
		  var regexp = /\B(?=(\d{3})+(?!\d))/g;
		  return num.toString().replace(regexp, ',');
		}


	/////////////////////보유금수정 END//////////////////////

	/////////////////////회원계좌조회 START//////////////////////
		function searchAccount(){
	
			name = $("#account_name").val();
			bankname = $("#account_bankname").val();
			bankaccount = $("#account_bankaccount").val();

		var obj = {
			"name":name,
			"bankname":bankname,
			"bankaccount":bankaccount,
			"task":"searchAccount",
			"site":"<%=site%>",
			"work":"<%=work%>"
		}
			
			
		$.post('/api/get_cs_work.asp',obj,function(data){
				
				data = JSON.parse(data);
		
				$("tr[id*=list7]").remove();
				$("tr[id*=empt7]").remove();
			

					if(data.length > 0 ){
						for(var key in data){							
							
							$("#clonebody7").clone()
										   .attr("id","lis7t"+key)
										   .appendTo("#layertbody7");
										

							$("#lis7t"+key+" .column0").text(data[key]["id"]);

						
						}

					$("tr[id*=lis7t]").removeAttr("style");
					amountUpdate();
				}else{

						$("#clonebody7").clone()
									   .attr("id","empt7y")
									   .appendTo("#layertbody7");

						$("#empt7y .column0").text("계좌내역이 없습니다.")
											.attr("colspan","1")
											.css("text-align","center");

						$("tr[id*=empt7y]").removeAttr("style");

					}

				});

		}
	/////////////////////회원계좌조회 END//////////////////////

	/////////////////////입출금 내역 START//////////////////////

	function searchDepoWid(){

		
		var sortType = $(".depowid option:selected").val();
		var userId = $("#userId3").val();

		var obj = {
			"sortType":sortType,
			"userId":userId,
			"startDt":$("#sd3").val(),
			"endDt":$("#sd4").val(),
			"site":"<%=site%>",
			"work":"<%=work%>"
		}

		$.post('/api/get_cs_work.asp',obj,function(data){
			
			data = JSON.parse(data);

			$("tr[id*=lis6t]").remove();
			$("tr[id*=empt6y]").remove();

			if(data.length > 0 ){
				for(var key in data){
					
					$("#clonebodydepo").clone()
								   .attr("id","lis6t"+key)
								   .appendTo("#layertbodydepo");

					var typetxt

					if(data[key]["type"] == "D"){
						typetxt = "입금";
					}else{
						typetxt = "출금";
					}

					var statetxt

					if(data[key]["state"] =="1"){
						statetxt = "대기"
					}else if(data[key]["state"] =="2"){
						statetxt = "완료"
					}else if(data[key]["state"] =="3"){
						statetxt = "관리자취소"
					}

 
					$("#lis6t"+key+" .column0").text(typetxt);
					$("#lis6t"+key+" .column1").text(data[key]["id"]);
					$("#lis6t"+key+" .column2").text(data[key]["name"]);
					$("#lis6t"+key+" .column3").text(addComma(data[key]["amount"]));
					$("#lis6t"+key+" .column4").text(data[key]["regdate"]);
					$("#lis6t"+key+" .column5").text(data[key]["updatedate"]);
					$("#lis6t"+key+" .column6").text(statetxt);

					$("#lis6t"+key).unbind("click").bind("click",function(){
				
						if($(this).hasClass("selectedLine")){
							$(this).removeClass("selectedLine");
						}else{
							$(this).addClass("selectedLine");
						}

					})

				}

				$("tr[id*=lis6t]").removeAttr("style");

			}else{

					$("#clonebodydepo").clone()
								   .attr("id","empt6y")
								   .appendTo("#layertbodydepo");

					$("#empt6y .column0").text("데이터가 없습니다")
										.attr("colspan","7")
										.css("text-align","center");

					$("tr[id*=empt6y]").removeAttr("style");

					for(var i=1; i<7; i++){
		
						$("#empt6y .column"+i).remove();

					}
				}
			});

			$(".excelDownBtn2").unbind("click").bind("click",function(){
				excelDown2(obj);
			});

	}
	function excelDown2(obj){

	var gameType = obj.gameType;
	var top		 = obj.top;
	var roomNo   = obj.roomNo;
	var sortType = obj.sortType;
	var gameNo   = obj.gameNo 
	var startDt  = obj.startDt;
	var endDt    = obj.endDt;
	var userId   = obj.userId;
	var roomOrder   = obj.roomOrder;
	var site     = obj.site;
	var work     = obj.work;
		if(confirm(' 배팅로그를 다운받으시겠습니까?')){
		location.href = "/api/get_cs_work.asp?sortType="+sortType+"&startDt="+startDt+"&endDt="+endDt+"&userId="+userId+"&site="+site+"&work="+work+"&task=excelDown";
		}

	}

	/////////////////////입출금 내역 END//////////////////////

	///////////////////// 회원삭제 START /////////////////////////

	function searchDelUser(){
	
	var userId

		if($("#userId5").val() == ""){
			userId = "null";
		}else{
			userId = $("#userId5").val();
		}

		
		var obj = {
			"userId":userId,
			"task":"searchDelUser",
			"site":"<%=site%>",
			"work":"<%=work%>"
		}

		$.post('/api/get_cs_work.asp',obj,function(data){
			
			data = JSON.parse(data);


			$("tr[id*=list]").remove();
			$("tr[id*=empty]").remove();

				if(data.length > 0 ){
					for(var key in data){

						$("#clonebody10").clone()
									   .attr("id","list"+key)
									   .appendTo("#layertbody10");
									
						$("#list"+key+" .column0").text(data[key]["id"]);
						$("#list"+key+" .column1").text(data[key]["guid"]);
						$("#list"+key+" .column2").text(data[key]["nickname"]);
						//$("#list"+key+" .column3 #userAmount2").val(data[key]["amount"]);
						$("#list"+key+" .column3 ").attr("id","userDelBtn");



					}

				$("tr[id*=list]").removeAttr("style");

			}else{

					$("#clonebody10").clone()
								   .attr("id","empty")
								   .appendTo("#layertbody10");

					$("#empty .column0").text("해당유저가 없습니다.")
										.attr("colspan","4")
										.css("text-align","center");

					$("tr[id*=empty]").removeAttr("style");

					for(var i=1; i<4; i++){
						$("#empty .column"+i).remove();
					}
				}
			});
		
	}

	///////////////////// 회원삭제 END /////////////////////////





</script>


<body style = "background:rgba(0, 0, 0, 0);">

<div style = "    float: left;
    width: 100%;
    padding: 20px;
    font-size: 27px;
    font-weight: bold;
    background: #0027b5;
    color: white;
    text-shadow: 0px 0px 2px black;    TEXT-ALIGN: CENTER;" class = 'center'> <%=site%>&nbsp;<%=work%></div>

	<!--////////////////////배팅로그조회 FORM START////////////////////// -->

	<div id="betlog">

		<div style = "     float: left;
		width: 100%;
		margin-top: 54px;
		height: 31px;
		background: #c9c9ce9e;">
		
		</div>
				
				
				
						<div class="">
							<table style="border-collapse: collapse;">
								<thead>
									<tr class="head">
										<th class="column1" style="width:50px;">id</th>
										<th class="column2" style="width:250px;">idx</th>
										<th class="column3" style="width:210px;" title="해당회원의 고유값">guid</th>
										<th class="column4" style="width:80px;" title="방 번호">roomno</th>
										<th class="column5" style="width:110px;" title="회차">roomorder</th>
										<th class="column6" style="width:30px;" title="실제유저가 베팅한 표를 표기하는것">bet</th>
										<th class="column7" style="width:30px;" title="해당회차의 결과값">result</th>
										<th class="column8" style="width:200px;" title="실제 유저가 배팅확인을 누른시간">reqdate</th>
										<th class="column9" style="width:200px;" title="결과값이 나온시간">resultdate</th>
										<th class="column10" style="width:80px;" title="유저가 베팅을 한 금액">betamount</th>
										<th class="column11" style="width:15px;">state</th>
										<th class="column12" style="width:70px;" title="유저가 배팅전 보유금액">pre_acc</th>
										<th class="column13" style="width:70px;" title="베팅후 보유금액">after_acc</th>
										<th class="column14" style="width:70px;" title="배당금">winprice</th>
										<th class="column15" style="width:30px;">bat</th>
										<th class="column16" style="width:50px;">q</th>
										<th class="column17" style="width:200px;" title="베팅칩을 올린시간">rdate</th>
									</tr>
								</thead>
							
								<tbody id="layertbody">
									<tr id="clonebody" style="display:none">
										<td class="column0"></td>
										<td class="column1"></td>
										<td class="column2"></td>
										<td class="column3"></td>
										<td class="column4"></td>
										<td class="column5"></td>
										<td class="column6"></td>
										<td class="column7"></td>
										<td class="column8"></td>
										<td class="column9"></td>
										<td class="column10"></td>
										<td class="column11"></td>
										<td class="column12"></td>
										<td class="column13"></td>
										<td class="column14"></td>
										<td class="column15"></td>
										<td class="column16"></td>
									</tr>

								</tbody>
							</table>
						</div>




		<div style = "    position: absolute;
		top: 90px;
		color: black;
		height: 32px;
		width: 106px;
		text-align: center;">검색조건</div>

		<div style = "    position: absolute;
		top: 90px;
		left: 100px;
		height: 32px;
		width: 850px;
		text-align: center;">

			<select class="top" style="width: 100px; height:22px;" >
			  <option value="top 500">최근 500개</option>
			  <option value="" >전체</option>
			</select>

			<select class="gameType" style="width: 100px; height:22px;" onchange="gameChange('betlog')">
			  <option value="midas">Midas</option>
			  <option value="88">88</option>
			  <option value="dragon">dragon</option>
			  <option value="mdi">MidasIn</option>
			  <option value="OG">OG</option>
			  <option value="EVO">EVOLUTION</option>
			  <option value="PP">PACIFIC</option>
	
			</select>

			<select class="roomNo" style="width: 100px; height:22px;" onchange="gameOrder('betlog')">
			  <option value="" id="roomAllbetlog">전체</option>
			</select>


			<select class="gameNo" style="width: 100px; height:22px;">
			  <option value="" id="gameAllbetlog">전체</option>
			</select>

			<select class="sortType" style="width: 100px; height:22px;">
			  <option value="bet">배팅</option>
			  <option value="cancel">취소</option>
			  <option value="scancel">시스템취소</option>
			</select>
			&nbsp;아이디
			<input id="userId" type="text" style="width:100px;"></input>
			&nbsp;회차
			<input id="condRoomOrder" type="text" style="width:100px;"></input>
		</div>


		<div style = "    position: absolute;
		top: 84px;
		background: rgba(12, 12, 232, 0.68);
		color: white;
		height: 24px;
		width: 69px;
		left: 1320px;
		padding-top: 8px;
		text-align: center;
		font-weight: bold;
		border-radius: 6px;
		cursor:pointer;" class = "searchBtn">조회</div>

<!--엑셀 다운 버튼 -->
		<div style = "    position: absolute;
		top: 84px;
		background: rgba(12, 12, 232, 0.68);
		color: white;
		height: 24px;
		width: 69px;
		left: 1410px;
		padding-top: 8px;
		text-align: center;
		font-weight: bold;
		border-radius: 6px;
		cursor:pointer;" class = "excelDownBtn">엑셀다운</div>

		<fieldset style = "    position: absolute;
		top: 82px;
		left: 950px;
		height: 29px;
		width: 314px;">
			<legend>기간 검색</legend>
			<div class = 's_d'>시작</div>
			<input type ="date" class = 's_d1' id = "sd1">
			<div class = 's_d'>종료</div>
			<input type ="date" class = 's_d1' id = "sd2">
		</fieldset>

	</div>

	
	<div class = "regDim center">
		<div class="regwrp" style="position: absolute;
									background: rgba(255, 255, 255);
									width: 460px;
									height: 500px;
									top: 20%;
									left: 0%;
									
									overflow: scroll;">

							<table style="border-collapse: collapse;">
								<thead>
									<tr class="head">
										<th class="column0" style="width:10%;">roomno</th>
										<th class="column1" style="width:10%;">data</th>
										<th class="column2" style="width:15%;">roomorder</th>
										<th class="column3" style="width:10%;">result</th>
										<th class="column4" style="width:10%;">state</th>
										<th class="column5" style="width:25%;">card</th>
										<th class="column6" style="width:40%;">reqdate</th>
									</tr>
								</thead>
							
								<tbody id="layertbody6">
									<tr id="clonebody6">
										<td class="column0" style="font-size:13px;"></td>
										<td class="column1" style="font-size:13px;"></td>
										<td class="column2" style="font-size:13px;"></td>
										<td class="column3" style="font-size:13px;"></td>
										<td class="column4" style="font-size:13px;"></td>
										<td class="column5" style="font-size:13px;"></td>
										<td class="column6" style="font-size:13px;"></td>
									</tr>

								</tbody>
							</table>
		
		</div>
	</div>

	<!--////////////////////배팅로그조회 FORM END////////////////////// -->

	<!--////////////////////회차결과내역@ FORM START////////////////////// -->

	<div id="roomrow">

		<div style = "     float: left;
		width: 100%;
		margin-top: 54px;
		height: 31px;
		background: #c9c9ce9e;">
		
		</div>
				
				
				
						<div class="">
							<table style="border-collapse: collapse;">
								<thead>
									<tr class="head">
										<th class="column0" style="width:10px;">roomno</th>
										<th class="column1" style="width:260px;">data</th>
										<th class="column2" style="width:200px;">reqdate</th>
										<th class="column3" style="width:70px;">roomorder</th>
										<th class="column4" style="width:20px;">result</th>
										<th class="column5" style="width:20px;">state</th>
										<th class="column6" style="width:240px;">card</th>
  
									</tr>
								</thead>
							
								<tbody id="layertbodyRoomrow">
									<tr id="clonebodyRoomrow" style="display:none">
										<td class="column0"></td>
										<td class="column1"></td>
										<td class="column2"></td>
										<td class="column3"></td>
										<td class="column4"></td>
										<td class="column5"></td>
										<td class="column6"></td>

									</tr>

								</tbody>
							</table>
						</div>




		<div style = "    position: absolute;
		top: 90px;
		color: black;
		height: 32px;
		width: 106px;
		text-align: center;">검색조건</div>

		<div style = "    position: absolute;
		top: 90px;
		left: 100px;
		height: 32px;
		width: 850px;
		text-align: center;">

			<select class="gameType" style="width: 100px; height:22px;" onchange="gameChange('roomrow')">
			  <option value="midas">Midas</option>
			  <option value="88">88</option>
			  <option value="dragon">dragon</option>
			  <option value="mdi">mdi</option>
			  <option value="OG">OG</option>

			</select>

			<select class="roomNo" style="width: 100px; height:22px;" onchange="gameOrder('roomrow')">
			  <option value="" id="roomAllroomrow">전체</option>
			</select>

 
			<select class="gameNo" style="width: 100px; height:22px;">
			  <option value="" id="gameAllroomrow">전체</option>
			</select>

			&nbsp;회차
			<input id="condRoomOrderRoomrow" type="text" style="width:100px;"></input>
		</div>
 
		<div style = "    position: absolute;
		top: 84px;
		background: rgba(12, 12, 232, 0.68);
		color: white;
		height: 24px;
		width: 69px;
		left: 790px;
		padding-top: 8px;
		text-align: center;
		font-weight: bold;
		border-radius: 6px;
		 
		cursor:pointer;" class = "searchBtn3">조회</div>
	

	</div>

	<!--////////////////////회차결과내역 FORM END////////////////////// -->

	<!--////////////////////보유금수정 FORM START////////////////////// -->

	<div id="modiamount">

		<div style = "    position: absolute;
						top: 90px;
						left: 0px;
						height: 32px;
						width: 850px;
						text-align: center;">
			아이디 조회
			<input id="userId2" type="text"></input>
		</div>

		<div style = "    position: absolute;
						top: 140px;
						left: 100px;
						height: 32px;
						width: 850px;
						text-align: left;" id="calculate">
		
		</div>

		<div style = "    position: absolute;
		top: 84px;
		background: rgba(12, 12, 232, 0.68);
		color: white;
		height: 24px;
		width: 69px;
		left: 570px;
		padding-top: 8px;
		text-align: center;
		font-weight: bold;
		border-radius: 6px;
		cursor:pointer;" class = "searchBtn2">조회</div>

			<div style = "    position: absolute; top: 175px;">
				<table style="border-collapse: collapse;">
					<thead>
						<tr class="head2">
							<th class="column0" style="width:220px; background:#80808047; text-align:center;">id</th>
							<th class="column1" style="width:220px; background:#80808047; text-align:center;">guid</th>
							<th class="column2" style="width:220px; background:#80808047; text-align:center;">닉네임</th>
							<th class="column3" style="width:220px; background:#80808047; text-align:center;">현재보유금</th>
						</tr>
					</thead>
				
					<tbody id="layertbody2">
						<tr id="clonebody2" style="display:none">
							<td class="column0"></td>
							<td class="column1"></td>
							<td class="column2"></td>
							<td class="column3" style="text-align:left;">
							<input type="text" id="userAmount" style="width:130px;"/>
							<div style = "    position: absolute;
								top: 23px;
								background: rgba(12, 12, 232, 0.68);
								color: white;
								height: 18px;
								width: 50px;
								left: 92%;
								padding-top: 8px;
								text-align: center;
								font-weight: bold;
								border-radius: 6px;
								cursor:pointer;" class = "modiAmountBtn">수정</div>
							
							
							
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div style = "    position: absolute; top: 235px;">
				<table style="border-collapse: collapse;">
					<thead>
						<tr class="head3">
							<th class="column0" style="width:220px; background:#80808047; text-align:center;">입금자</th>
							<th class="column1" style="width:220px; background:#80808047; text-align:center;">입금금액</th>
							<th class="column2" style="width:220px; background:#80808047; text-align:center;">상태</th>
							<th class="column3" style="width:220px; background:#80808047; text-align:center;">처리날짜</th>
						</tr>
					</thead>
				
					<tbody id="layertbody3">
						<tr id="clonebody3" style="display:none">
							<td class="column0"></td>
							<td class="column1"></td>
							<td class="column2"></td>
							<td class="column3"></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div style = "    position: absolute; top: 295px;">
				<table style="border-collapse: collapse;">
					<thead>
						<tr class="head4">
							<th class="column0" style="width:220px; background:#80808047; text-align:center;">출금자</th>
							<th class="column1" style="width:220px; background:#80808047; text-align:center;">출금금액</th>
							<th class="column2" style="width:220px; background:#80808047; text-align:center;">상태</th>
							<th class="column3" style="width:220px; background:#80808047; text-align:center;">처리날짜</th>
						</tr>
					</thead>
				
					<tbody id="layertbody4">
						<tr id="clonebody4" style="display:none">
							<td class="column0"></td>
							<td class="column1"></td>
							<td class="column2"></td>
							<td class="column3"></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div style = "    position: absolute; top: 355px;">
				<table style="border-collapse: collapse;">
					<thead>
						<tr class="head5">
							<th class="column0" style="width:110px; background:#80808047; text-align:center;">게임타입</th>
							<th class="column1" style="width:110px; background:#80808047; text-align:center;">회차</th>
							<th class="column2" style="width:130px; background:#80808047; text-align:center;">배팅금</th>
							<th class="column3" style="width:130px; background:#80808047; text-align:center;">배팅전</th>
							<th class="column4" style="width:130px; background:#80808047; text-align:center;">배팅후</th>
							<th class="column5" style="width:130px; background:#80808047; text-align:center;">적중금</th>
							<th class="column6" style="width:210px; background:#80808047; text-align:center;">배팅시각</th>
						</tr>
					</thead>
				
					<tbody id="layertbody5">
						<tr id="clonebody5" style="display:none">
							<td class="column0"></td>
							<td class="column1"></td>
							<td class="column2"></td>
							<td class="column3"></td>
							<td class="column4"></td>
							<td class="column5"></td>
							<td class="column6"></td>
						</tr>
					</tbody>
				</table>
			</div>


	</div>

	<!--////////////////////보유금수정 FORM END////////////////////// -->

	<!--////////////////////회원계좌조회 FORM START////////////////////// -->

	<div id="memberaccount">

		<div style = "    position: absolute;
						top: 90px;
						left: 0px;
						height: 32px;
						width: 850px;
						text-align: center;">
			예금주
			<input id="account_name" type="text"></input>
			은행
			<input id="account_bankname" type="text"></input>
			계좌번호
			<input id="account_bankaccount" type="text"></input>
		</div>

		<div style = "    position: absolute;
						top: 140px;
						left: 100px;
						height: 32px;
						width: 850px;
						text-align: left;" id="calculate">
		
		</div>

		<div style = "    position: absolute;
		top: 134px;
		background: rgba(12, 12, 232, 0.68);
		color: white;
		height: 24px;
		width: 69px;
		left: 570px;
		padding-top: 8px;
		text-align: center;
		font-weight: bold;
		border-radius: 6px;
		cursor:pointer;" class = "searchBtn7">조회</div>



			<div style = "    position: absolute; top: 235px;">
				<table style="border-collapse: collapse;">
					<thead>
						<tr class="head7">
							<th class="column0" style="width:220px; background:#80808047; text-align:center;">아이디</th>
						</tr>
					</thead>
				
					<tbody id="layertbody7">
						<tr id="clonebody7" style="display:none">
							<td class="column0"></td>

						</tr>
					</tbody>
				</table>
			</div>

		



	</div>

	<!--////////////////////회원계좌조회 FORM END////////////////////// -->

	<!--////////////////////회원입금내역 FORM START////////////////////// -->

	<div id="userdeposit">

		<div style = "     float: left;
		width: 100%;
		margin-top: 54px;
		height: 31px;
		background: #c9c9ce9e;">
		
		</div>
				
						<div class="">
							<table style="border-collapse: collapse;">
								<thead>
									<tr class="head">
										<th class="column0" style="width:150px;">type</th>
										<th class="column1" style="width:150px;">id</th>
										<th class="column2" style="width:150px;">name</th>
										<th class="column3" style="width:150px;">amount</th>
										<th class="column4" style="width:250px;">regdate</th>
										<th class="column5" style="width:250px;">updatedate</th>
										<th class="column6" style="width:100px;">state</th>
									</tr>
								</thead>
							
								<tbody id="layertbodydepo">
									<tr id="clonebodydepo" style="display:none">
										<td class="column0"></td>
										<td class="column1"></td>
										<td class="column2"></td>
										<td class="column3"></td>
										<td class="column4"></td>
										<td class="column5"></td>
										<td class="column6"></td>
									</tr>

								</tbody>
							</table>
						</div>


		<div style = "    position: absolute;
		top: 90px;
		color: black;
		height: 32px;
		width: 106px;
		text-align: center;">검색조건</div>

		<div style = "    position: absolute;
		top: 90px;
		left: 100px;
		height: 32px;
		width: 350px;
		text-align: center;">

			<select class="depowid" style="width: 100px; height:22px;">
			  <option value="all">전체</option>
			  <option value="deposit">입금</option>
			  <option value="widraw">출금</option>
			</select>

			&nbsp;아이디
			<input id="userId3" type="text" style="width:100px;"></input>
		</div>

		<fieldset style = "    position: absolute;
		top: 82px;
		left: 420px;
		height: 29px;
		width: 314px;">
			<legend>기간 검색</legend>
			<div class = 's_d'>시작</div>
			<input type ="date" class = 's_d1' id = "sd3">
			<div class = 's_d'>종료</div>
			<input type ="date" class = 's_d1' id = "sd4">
		</fieldset>
 
		<div style = "    position: absolute;
		top: 84px;
		background: rgba(12, 12, 232, 0.68);
		color: white;
		height: 24px;
		width: 69px;
		left: 790px;
		padding-top: 8px;
		text-align: center;
		font-weight: bold;
		border-radius: 6px;
		cursor:pointer;" class = "searchBtn4">조회</div>

		<div style = "    position: absolute;
		top: 84px;
		background: rgba(12, 12, 232, 0.68);
		color: white;
		height: 24px;
		width: 69px;
		left: 880px;
		padding-top: 8px;
		text-align: center;
		font-weight: bold;
		border-radius: 6px;
		cursor:pointer;" class = "excelDownBtn2">엑셀다운</div>
	

	</div>

	<!--////////////////////회원입금내역 FORM END////////////////////// -->
	<!--////////////////////회원삭제  START ////////////////////// -->

	<div id="deluser">

		<div style = "    position: absolute;
						top: 90px;
						left: 0px;
						height: 32px;
						width: 850px;
						text-align: center;">
			아이디 조회
			<input id="userId5" type="text"></input>
		</div>

		<div style = "    position: absolute;
						top: 140px;
						left: 100px;
						height: 32px;
						width: 850px;
						text-align: left;" id="calculate">
		
		</div>

		<div style = "    position: absolute;
		top: 84px;
		background: rgba(12, 12, 232, 0.68);
		color: white;
		height: 24px;
		width: 69px;
		left: 570px;
		padding-top: 8px;
		text-align: center;
		font-weight: bold;
		border-radius: 6px;
		cursor:pointer;" class = "searchBtn5">조회</div>

			<div style = "    position: absolute; top: 175px;">
				<table style="border-collapse: collapse;">
					<thead>
						<tr class="head10">
							<th class="column0" style="width:220px; background:#80808047; text-align:center;">id</th>
							<th class="column1" style="width:220px; background:#80808047; text-align:center;">guid</th>
							<th class="column2" style="width:220px; background:#80808047; text-align:center;">닉네임</th>
							<th class="column3" style="width:220px; background:#80808047; text-align:center;">회원삭제</th>
						</tr>
					</thead>
				
					<tbody id="layertbody10">
						<tr id="clonebody10" style="display:none">
							<td class="column0"></td>
							<td class="column1"></td>
							<td class="column2"></td>
							<td class="column3" style="text-align:left;">

							<div style = "  position: absolute;
											top: 25px;
											background: rgba(169, 7, 82, 0.68);
											color: white;
											height: 30px;
											width: 68px;
											left: 84%;
											padding-top: 8px;
											text-align: center;
											font-weight: bold;
											border-radius: 6px;
											cursor: pointer;" class = "userDelBtn">삭제</div>
							
							
							
							</td>
						</tr>
					</tbody>
				</table>
			</div>


	</div>

	<!--////////////////////회원삭제  START END////////////////////// -->

</body>
</html>